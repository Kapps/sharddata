﻿module ShardData.MySql.MySqlTransaction;
import ShardData.MySql.MySqlConnection;

import ShardData.DbTransaction;

/// Represents a transaction on a MySql database.
class MySqlTransaction : DbTransaction {

public:
	/// Initializes a new instance of the MySqlTransaction object.
	this(MySqlConnection Connection) {
		super(Connection);
	}
	
private:
}