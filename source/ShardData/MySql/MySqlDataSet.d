﻿module ShardData.MySql.MySqlDataSet;
import ShardTools.Logger;

import ShardData.DbParameter;
import ShardData.MySql.MySqlConnection;
import ShardData.MySql.MySqlDefines;
import ShardTools.IDisposable;

import ShardData.DbDataSet;
import ShardData.DataRow;
import std.exception;
import std.conv : to;

/// Represents a collection of rows returned from a MySql query.
final class MySqlDataSet : DbDataSet, IDisposable {

public:
	/// Initializes a new instance of the MySqlDataSet object.
	package this(MYSQL_RES* ResHandle, MySqlConnection Connection) {
		super(Connection);
		this._IsDisposed = false;
		this.ResHandle = ResHandle;
		this.TotalRows = mysql_affected_rows(Connection.Handle);
		CheckResult(TotalRows != -1, "Error in fetching rows from set.", Connection);
	}

	/// Initializes a new instance of the MySqlDataSet object.
	package this(MYSQL_STMT* StmtHandle, MySqlParameter[] Params, MySqlConnection Connection) {
		super(Connection);
		this._IsDisposed = false;
		this.StmtHandle = StmtHandle;	
		this.TotalRows = mysql_stmt_affected_rows(StmtHandle);
		CheckStmtResult(TotalRows != -1, "Error in fetching rows from set.");		
		this.Params = Params;
	}

	/// Reads a row and advances to the next row.
	override DataRow Read() {		
		if(this.RowIndex >= TotalRows)
			return DataRow();
		this.RowIndex++;
		ubyte[][string] Map;
		if(this.ResHandle) {
			MYSQL_ROW row = mysql_fetch_row(ResHandle);		
			//Map.length = _FieldCount;
			uint* lengths = mysql_fetch_lengths(ResHandle);
			for(uint i = 0; i < _FieldCount; i++) {
				MYSQL_FIELD field = Fields[i];
				uint NameLength = field.name_length;
				uint ValueLength = lengths[i];
				string Name = field.name[0 .. field.name_length].idup;
				ubyte[] Value = cast(ubyte[])row[i][0..ValueLength].dup;
				Map[Name] = Value;
			}
		} else if(this.StmtHandle) {
			int FetchResult = mysql_stmt_fetch(this.StmtHandle);
			CheckStmtResult(FetchResult == 0, "Unable to fetch statement");			
			foreach(MySqlParameter Param; Params) {
				if(Param.Direction == ParameterDirection.Out || Param.Direction == ParameterDirection.InOut || Param.Direction == ParameterDirection.Return) {
					ubyte[] Value = Param.Value!(ubyte[])().dup;					
					Map[Param.Name] = Value;			
				}
			}
		} else
			throw new Exception("Unknown data set handle.");
		return DataRow(Map);
	}

	/// Occurs when this object has Dispose called when it was not already disposed.
	protected void OnDispose() {
		if(this.ResHandle)
			mysql_free_result(ResHandle);
		if(this.StmtHandle)
			mysql_stmt_free_result(StmtHandle);
	}

	/// Gets a value whether this object is currently disposed.
	@property nothrow bool IsDisposed() const {
		return _IsDisposed;
	}

	/// Gets the number of fields in the set.
	@property const uint FieldCount() {
		return _FieldCount;
	}

	~this() {
		Dispose();
	}

	int opApply(int delegate(ref DataRow) Operation) {
		int Result = 0;
		while(this.RowIndex < TotalRows) {
			DataRow Row = Read();			
			if((Result = Operation(Row)) != 0)
				break;
		}
		return Result;	
	}
	
private:
	bool _IsDisposed;	
	// TODO: Can union this if needed. Probably can't actually, without storing which is used. Which defeats the purpose.
	MYSQL_RES* ResHandle;	
	MYSQL_FIELD* Fields;
	uint _FieldCount;
	MYSQL_STMT* StmtHandle;
	MySqlParameter[] Params;
	ulong TotalRows, RowIndex;

	private void CheckStmtResult(T)(T value, lazy string Error) {		
		if(!value) {			
			char* ErrorUncasted = cast(char*)mysql_stmt_error(StmtHandle);
			if(*ErrorUncasted == '\0')
				ErrorUncasted = cast(char*)mysql_error(StmtHandle.mysql);					
			string ErrorMessage = to!string(ErrorUncasted);			
			string Message = Error ~ " " ~ ErrorMessage ~ " Result was " ~ to!string(value) ~ ".";
			Log("errors", Message);
			throw new MySqlException(Message);
		}
	}
}