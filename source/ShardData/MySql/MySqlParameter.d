﻿module ShardData.MySql.MySqlParameter;
import std.traits;
import ShardData.MySql.MySqlDefines;
import std.stdio : writefln;
import ShardData.DbParameter;

enum MySqlParamType { 
	Decimal,
	Tiny,
	Short,
	Integer,
	Float,
	Double,
	Null,
	Timestamp,
	Long,
	Int24,
	Date,
	Time,
	DateTime,
	Year,
	NewDate,
	VarChar,
	Bit,
	NewDecimal=246,
	Enum=247,
	Set=248,
	TinyBlob=249,
	MediumBlob=250,
	LongBlob=251,
	Blob=252,
	VarString=253,
	String=254,
	Geometry=255
};

/// Represents a parameter for a MySqlCommand.
final class MySqlParameter : DbParameter {

public:

	/// Determines the type of this parameter.
	const MySqlParamType ParamType;

	/// Initializes a new instance of the MySqlParameter object for input purposes.
	this(MySqlParamType ParamType, string Name) {
		super(Name);
		this.ParamType = ParamType;	
	}	

	/// Initializes a new instance of the MySqlParameter object for output purposes.
	/// Params:
	/// 	ParamType = The type of the parameter.
	/// 	Name = The name of the parameter.
	/// 	Direction = The direction. A value of in indicates being passed to the Sql Server, while Out indicates receiving data from it.
	/// 	SizeInBytes = The size, in bytes, of the buffer to use for the value.
	this(MySqlParamType ParamType, string Name, ParameterDirection Direction, uint SizeInBytes) {
		super(Name, Direction, SizeInBytes);
		this.ParamType = ParamType;		
	}	

	protected override void OnValueAccessed() {
		if(Bind && !AssigningValue)
			this.ValueSize = *Bind.length;
	}

	protected override void OnValueChanged() {
		if(Bind) {
			AssigningValue = true;
			try {
				ubyte[] Bytes = (this.Value!(ubyte[]))();
				Bind.buffer = Bytes.ptr;
				Bind.buffer_length = BufferSize;
				*Bind.length = ValueSize;
			} finally {
				AssigningValue = false;
			}
		}
	}
	
	package MYSQL_BIND* Bind;
	
private:
	bool AssigningValue = false;
}