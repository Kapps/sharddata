﻿module ShardData.MySql.MySqlConnection;
private import ShardData.ConnectionPool;
import ShardTools.ExceptionTools;
import ShardTools.Logger;
import ShardData.Query;
import ShardData.DbParameter;
import std.conv;

public import ShardData.MySql.MySqlConnectionInfo;
public import ShardData.DbConnection;
public import ShardData.MySql.MySqlCommand;
public import ShardData.MySql.MySqlParameter;
public import ShardData.MySql.MySqlTransaction;
private import ShardData.MySql.MySqlDefines;
private import std.string;
import std.exception;

mixin(MakeException("MySqlException", "An error has occurred when calling a MySQL function."));

alias ConnectionPool!(MySqlConnection, MySqlConnectionInfo) MySqlConnectionPool;

/// Represents a connection to a MySql database.
final class MySqlConnection  : DbConnection {

public:
	/// Initializes a new instance of the MySqlConnection object.
	this(MySqlConnectionInfo ConnectionInfo...) {
		super(ConnectionInfo);		
	}

protected:

	override ulong PerformRawQuery(string Query) {
		if(mysql_query(Handle, tocs(Query)))
			throw new CommandFailedException("Unable to execute query. Details: " ~ to!string(mysql_error(Handle)));
		return mysql_affected_rows(Handle);
	}

	override void PerformOpen() {
		MySqlConnectionInfo ci = cast(MySqlConnectionInfo)this.ConnectionInfo;		
		Handle = enforce(mysql_init(Handle), "Unable to initialize MySQL connection.");
		//Handle.reconnect = 1;		
		my_bool* reconnect = new my_bool();
		*reconnect = 1;
		enforce(mysql_options(Handle, mysql_option.MYSQL_OPT_RECONNECT, reconnect) == 0, "Unable to set automatic reconnecting.");
		if(ci.SSL == SslMode.Prefer || ci.SSL == SslMode.Require)
			mysql_ssl_set(Handle, null, null, null, null, null); // Always returns 0.
		if(ci.UseCompression)
			enforce(mysql_options(Handle, mysql_option.MYSQL_OPT_COMPRESS, null) == 0, "Unable to enable compression."); // Arg unused.
		CheckResult(mysql_real_connect(Handle, tocs(ci.Host), tocs(ci.Username), tocs(ci.Password), tocs(ci.Database), ci.Port, "", 0), "Unable to connect.", this);		
		//if(mysql_select_db(Handle, tocs(ci.Database)))
			//throw new CommandFailedException("Unable to select database " ~ ci.Database ~ ". Details: " ~ to!string(mysql_error(Handle)));
	}
	
	override void PerformClose() {
		mysql_close(Handle);		
	}	

private:		
	package MYSQL* Handle;

	char* tocs(string Value) {
		immutable(char)* cstring = toStringz(Value.dup);
		return cast(char*)cstring;
	}
}

	package void CheckResult(T)(T value, lazy string Error, DbConnection Connection) {		
		if(!value) {			
			const(char*) ErrorUncasted = mysql_error((cast(MySqlConnection)Connection).Handle);			
			string ErrorMessage = to!string(ErrorUncasted);			
			string Message = Error ~ " " ~ ErrorMessage ~ " Result was " ~ to!string(value) ~ ".";
			Log("errors", Message);
			throw new MySqlException(Message);
		}
	}