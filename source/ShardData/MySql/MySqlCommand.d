﻿module ShardData.MySql.MySqlCommand;
import ShardTools.Logger;
import ShardData.DbParameter;
import ShardData.MySql.MySqlDefines;
import ShardData.MySql.MySqlDataSet;
import ShardData.DbDataSet;
import ShardData.MySql.MySqlConnection;
import std.exception;
import std.string;
import ShardTools.ExceptionTools;
import ShardData.DbCommand;
import ShardTools.List;
import std.stdio : writefln;
import std.conv;

mixin(MakeException("CommandPreparedException", "Unable to execute this operation when the command was already executed once, and thus prepared."));

/// Represents a command sent to a MySql database.
class MySqlCommand : DbCommand {

public:
	/// Initializes a new instance of the MySqlCommand object.
	this(MySqlConnection Connection, string CommandText = null) {
		super(Connection, CommandText);				
		IsPrepared = false;
	}

	this(MySqlConnection Connection, string CommandText, MySqlParameter[] Parameters...) {
		this(Connection, CommandText);
		foreach(MySqlParameter Param; Parameters) {
			AddParameter(Param);
		}
	}

	this(MySqlConnection Connection, Query query) {
		super(Connection, query);
	}

	this(MySqlConnection Connection, Query query, MySqlParameter[] Parameters...) {
		super(Connection, query);
		foreach(MySqlParameter Param; Parameters)
			AddParameter(Param);
	}

	/// Gets or sets the command text to execute.
	@property override void CommandText(string Text) {
		if(IsPrepared)
			throw new CommandPreparedException();
		super.CommandText(Text);
	}

	/// Ditto
	@property override string CommandText() {
		return super.CommandText();
	}

	/// Adds the given parameter to the command for execution.
	/// Params:
	/// 	Parameter = The parameter to add to the command.
	override void AddParameter(DbParameter Parameter) {
		MySqlParameter msqParam = cast(MySqlParameter)Parameter;
		enforce(msqParam);
		super.AddParameter(Parameter);		
	}
	
protected:

	override MySqlDataSet PerformExecuteSet() {
		HandleExecute();
		MySqlParameter[] Params = cast(MySqlParameter[])this.Parameters();
		return new MySqlDataSet(Handle, Params, cast(MySqlConnection)Connection);
	}

	override ulong PerformExecute() {
		return HandleExecute();
	}

	~this() {		
		if(Handle) {
			mysql_stmt_close(Handle);
			
		}
	}

	/// Populates this command with information from the given query.	
	protected override void PopulateFromQuery(Query query) {
		string Result = "";
		for(size_t i = 0; i < query.SelectClauses.length; i++)
			Result ~= query.SelectClauses[i] ~ ", ";
		if(Result != "")
			Result = Result[0..$-2];
		else
			Result = "*";
		Result = "SELECT " ~ Result;
		Result ~= " FROM " ~ query.Table;
		if(query.WhereClauses.length > 0) {
			Result ~= " WHERE " ~ query.WhereClauses[0];
			for(size_t i = 1; i < query.WhereClauses.length; i++)
				Result ~= " AND " ~ query.WhereClauses[i];
		}
		if(query.OrderClauses.length > 0) {
			Result ~= " ORDER BY " ~ query.OrderClauses[0];
			for(size_t i = 1; i < query.OrderClauses.length; i++)
				Result ~= ", " ~ query.OrderClauses[i];
		}
		if(query.Start) { // Start and Count are set with the same function, if one is not null both are.
			Result ~= " LIMIT " ~ to!string(query.Start) ~ ", " ~ to!string(query.Count);
		}
		Result ~= ";";
		this.CommandText = Result;		
	}

	unittest {
		Query query = Query("TestTable").Where("a > 2");
		MySqlCommand Com = new MySqlCommand(null, null);
		Com.PopulateFromQuery(query);		
		assert(Com.CommandText == "SELECT * FROM TestTable WHERE a > 2;");
		Com.PopulateFromQuery(query.Limit("2", "10"));
		assert(Com.CommandText == "SELECT * FROM TestTable WHERE a > 2 LIMIT 2, 10;");
		Com.PopulateFromQuery(query.Select("TestField"));
		assert(Com.CommandText == "SELECT TestField FROM TestTable WHERE a > 2 LIMIT 2, 10;");
		Com.PopulateFromQuery(query.Order("TestField DESC"));
		assert(Com.CommandText == "SELECT TestField FROM TestTable WHERE a > 2 ORDER BY TestField DESC LIMIT 2, 10;");
		Com.PopulateFromQuery(query.Select("TestField2").Order("TestField2 DESC"));
		assert(Com.CommandText == "SELECT TestField, TestField2 FROM TestTable WHERE a > 2 ORDER BY TestField DESC, TestField2 DESC LIMIT 2, 10;");
	}
	
private:	
	bool WasExecuted;
	bool IsPrepared;	
	MYSQL_STMT* Handle;	
	MYSQL_BIND[] Outputs;
	MYSQL_BIND[] Inputs;

	void EnsurePrepared() {
		if(!IsPrepared) {			
			this.IsPrepared = true;			
			CheckResult(Handle = mysql_stmt_init((cast(MySqlConnection)Connection).Handle), "Unable to initialize statement.", Connection);
			string CommandText = this.CommandText();
			char* ComTextPtr = cast(char*)toStringz(CommandText);
			CheckStmtResult(!mysql_stmt_prepare(Handle, ComTextPtr, CommandText.length), "Unable to prepare statement.");			

			size_t InputCount, OutputCount;		
			this.Inputs = new MYSQL_BIND[Parameters.length];
			this.Outputs = new MYSQL_BIND[Parameters.length];
			MYSQL_BIND* Inputs = this.Inputs.ptr;		
			MYSQL_BIND* Outputs = this.Outputs.ptr;
			foreach(DbParameter _Param; Parameters) {
				MySqlParameter Param = cast(MySqlParameter)_Param;
				MYSQL_BIND Bind;			
				Bind.buffer_type = cast(enum_field_types)Param.ParamType;
				ubyte[] Values = Param.Value!(ubyte[])();
				Bind.buffer_length = Param.ValueSize;
				Bind.buffer = Values.ptr;
				//Bind.is_null_value = Values is null;
				char* IsNull = new char();
				*IsNull = Values is null;
				Bind.is_null = IsNull;
				size_t* Length = new size_t();
				*Length = Values.length;
				Bind.length = Length;
				if(Param.Direction == ParameterDirection.In) {
					Inputs[InputCount++] = Bind;				
					Param.Bind = &Inputs[InputCount - 1];
				} else if(Param.Direction != ParameterDirection.InOut) {
					Outputs[OutputCount++] = Bind;
					Param.Bind = &Outputs[OutputCount - 1];					
				} else {
					Inputs[InputCount++] = Bind;
					Outputs[OutputCount++] = Bind;
					Param.Bind = &Outputs[OutputCount - 1];					
				}			
			}					
			this.Inputs = this.Inputs[0..InputCount];
			this.Outputs = this.Outputs[0..OutputCount];				
			CheckStmtResult(!mysql_stmt_bind_param(Handle, this.Inputs.ptr), "Unable to bind parameters.");	
		}
	}

	ulong HandleExecute() {					
		EnsurePrepared();				
		CheckStmtResult(!mysql_stmt_bind_param(Handle, this.Inputs.ptr), "Unable to bind parameters.");		
		CheckStmtResult(!mysql_stmt_execute(Handle), "Execution failed.");
		if(Outputs.length > 0) {
			CheckStmtResult(!mysql_stmt_bind_result(Handle, Outputs.ptr), "Unable to bind result: ");
			CheckStmtResult(!mysql_stmt_store_result(Handle), "Storing result failed.");
		}
		/+for(size_t i = 0; i < Handle.field_count; i++) {
			if(Parameters[i].Direction != ParameterDirection.In)
				(cast(MySqlParameter)Parameters[i]).Bind = &Handle.fields[i];
		}+/			
		return mysql_stmt_affected_rows(Handle);
	}

	private void CheckStmtResult(T)(T value, lazy string Error) {		
		if(!value) {			
			char* ErrorUncasted = cast(char*)mysql_stmt_error(Handle);
			if(*ErrorUncasted == '\0')
				ErrorUncasted = cast(char*)mysql_error(Handle.mysql);					
			string ErrorMessage = to!string(ErrorUncasted);			
			string Message = Error ~ " " ~ ErrorMessage ~ " Result was " ~ to!string(value) ~ ".";
			Log("errors", Message);
			throw new MySqlException(Message);
		}
	}
}