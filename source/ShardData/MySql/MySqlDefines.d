﻿module ShardData.MySql.MySqlDefines;
private import std.string;

	enum MYSQL_ERRMSG_SIZE = 512;
	enum SQLSTATE_LENGTH = 5;
	enum SCRAMBLE_LENGTH = 20;

	alias char my_bool;		
	alias const(ubyte)* cstring;
	alias char** MYSQL_ROW;

	enum enum_field_types { 
		MYSQL_TYPE_DECIMAL, MYSQL_TYPE_TINY,
        MYSQL_TYPE_SHORT,  MYSQL_TYPE_LONG,
        MYSQL_TYPE_FLOAT,  MYSQL_TYPE_DOUBLE,
        MYSQL_TYPE_NULL,   MYSQL_TYPE_TIMESTAMP,
        MYSQL_TYPE_LONGLONG,MYSQL_TYPE_INT24,
        MYSQL_TYPE_DATE,   MYSQL_TYPE_TIME,
        MYSQL_TYPE_DATETIME, MYSQL_TYPE_YEAR,
        MYSQL_TYPE_NEWDATE, MYSQL_TYPE_VARCHAR,
        MYSQL_TYPE_BIT,
        MYSQL_TYPE_NEWDECIMAL=246,
        MYSQL_TYPE_ENUM=247,
        MYSQL_TYPE_SET=248,
        MYSQL_TYPE_TINY_BLOB=249,
        MYSQL_TYPE_MEDIUM_BLOB=250,
        MYSQL_TYPE_LONG_BLOB=251,
        MYSQL_TYPE_BLOB=252,
        MYSQL_TYPE_VAR_STRING=253,
        MYSQL_TYPE_STRING=254,
        MYSQL_TYPE_GEOMETRY=255
	};

	enum mysql_status {
		MYSQL_STATUS_READY,MYSQL_STATUS_GET_RESULT,MYSQL_STATUS_USE_RESULT
	};

	enum mysql_option  {
		MYSQL_OPT_CONNECT_TIMEOUT, MYSQL_OPT_COMPRESS, MYSQL_OPT_NAMED_PIPE,
		MYSQL_INIT_COMMAND, MYSQL_READ_DEFAULT_FILE, MYSQL_READ_DEFAULT_GROUP,
		MYSQL_SET_CHARSET_DIR, MYSQL_SET_CHARSET_NAME, MYSQL_OPT_LOCAL_INFILE,
		MYSQL_OPT_PROTOCOL, MYSQL_SHARED_MEMORY_BASE_NAME, MYSQL_OPT_READ_TIMEOUT,
		MYSQL_OPT_WRITE_TIMEOUT, MYSQL_OPT_USE_RESULT,
		MYSQL_OPT_USE_REMOTE_CONNECTION, MYSQL_OPT_USE_EMBEDDED_CONNECTION,
		MYSQL_OPT_GUESS_CONNECTION, MYSQL_SET_CLIENT_IP, MYSQL_SECURE_AUTH,
		MYSQL_REPORT_DATA_TRUNCATION, MYSQL_OPT_RECONNECT,
		MYSQL_OPT_SSL_VERIFY_SERVER_CERT
	};

	struct LIST {
		LIST* prev, next;
		void *data;
	}

	struct NET {
		ubyte* unused;
		uint last_errno;
		ubyte error;
		my_bool unused4, unused5;
		char[MYSQL_ERRMSG_SIZE] last_error;
		char[SQLSTATE_LENGTH + 1] sqlstate;
		void* extension;
	}		

	struct MYSQL_DATA {
		MYSQL_ROWS* data;
		void* embedded_info; // embedded_query_result
		MEM_ROOT alloc;
		ulong rows;
		uint fields;
		void* extension;
	}

	struct MYSQL_BIND {		
		size_t* length;
		my_bool* is_null;
		void* buffer;
		my_bool* Error;
		ubyte* row_ptr;
		void function(NET*, MYSQL_BIND*) store_param_func;
		void function(MYSQL_BIND*, MYSQL_FIELD*, ubyte**) fetch_result;
		void function(MYSQL_BIND*, MYSQL_FIELD*, ubyte**) skip_result;
		size_t buffer_length, offset, length_value;
		uint param_number, pack_length;
		enum_field_types buffer_type;
		my_bool error_value, is_unsigned, long_data_used, is_null_value;
		void* extension;
	}

	enum enum_mysql_stmt_state {
		MYSQL_STMT_INIT_DONE= 1, MYSQL_STMT_PREPARE_DONE, MYSQL_STMT_EXECUTE_DONE,
		MYSQL_STMT_FETCH_DONE
	};

	enum enum_stmt_attr_type {
		STMT_ATTR_UPDATE_MAX_LENGTH,  
		STMT_ATTR_CURSOR_TYPE,
		STMT_ATTR_PREFETCH_ROWS
	};

	struct MYSQL_STMT {
		MEM_ROOT mem_root;
		LIST list;
		MYSQL* mysql;
		MYSQL_BIND* params, bind;
		MYSQL_FIELD* fields;
		MYSQL_DATA result;
		MYSQL_ROWS* data_cursor;
		int function(MYSQL_STMT stmt, ubyte** row) read_row_func;
		ulong affected_rows;
		ulong insert_id;
		size_t stmt_id;
		size_t flags;
		size_t prefetch_rows;
		uint server_status, last_errno, param_count, field_count;
		enum_mysql_stmt_state state;
		char[MYSQL_ERRMSG_SIZE] last_error;
		char[SQLSTATE_LENGTH+1] sqlstate;
		my_bool send_types_to_server;
		my_bool bind_param_done;
		ubyte bind_Result_done;
		my_bool unbuffered_fetch_cancelled;
		my_bool update_max_length;
		void* extension;
	}

	struct MYSQL_RES {
		ulong row_count;
		MYSQL_FIELD* fields;
		MYSQL_DATA* data;
		MYSQL_ROWS* data_cursor;
		size_t* lengths;
		MYSQL* handle;
		void* methods; // st_mysql_methods;
		MYSQL_ROW row;
		MYSQL_ROW current_row;
		MEM_ROOT field_alloc;
		uint field_count, current_field;
		my_bool eof;
		my_bool unbuffered_fetch_cancelled;
		void* extension;
	}

	struct MYSQL_ROWS {
		MYSQL_ROWS* next;
		MYSQL_ROW data;
		size_t length;
	}

	alias MYSQL_ROWS* MYSQL_ROW_OFFSET;
	
	struct MYSQL_FIELD {
		char* name;
		char*org_name;
		char*table;  
		char*org_table;
		char*db;    
		char*catalog;
		char*def;
		size_t length;
		size_t max_length;
		uint name_length;
		uint org_name_length;
		uint table_length;
		uint org_table_length;
		uint db_length;
		uint catalog_length;
		uint def_length;
		uint flags;
		uint decimals;
		uint charsetnr;
		enum_field_types type;
		void *extension;
	} 

	struct USED_MEM {
		USED_MEM* next;
		size_t left;
		size_t size;
	}

	struct MEM_ROOT {
		USED_MEM* free, used, pre_alloc;
		size_t min_alloc;
		size_t block_size;
		uint block_num;
		uint first_block_usage;
		void* function() error_handler;
	}

	struct st_mysql_options {
		uint connect_timeout, read_timeout, write_timeout, port, protocol;
		size_t client_flag;
		char* host, user, password, unix_socket, db;
		void* init_commands; // st_dynamic_array
		char* my_cnf_file, my_cnf_group, charset_dir, charset_name;
		char* ssl_key, ssl_cert, ssl_ca, ssl_capath, ssl_cipher;
		char* shared_memory_base_name;
		size_t max_allowed_packet;
		my_bool use_ssl, compress, named_pipe, unused1, unused2, unused3, unused4;
		mysql_option methods_to_use;
		char* client_ip;
		my_bool secure_auth;
		my_bool report_data_truncation;

		int function(void**, const char*, void*) local_infile_init;
		int function(void*, char*, uint) local_infile_read;
		void function(void*) local_infile_end;
		int function(void*, char*, uint) local_infile_error;
		void* local_infile_userdata;
		void* extension;
	}

	struct MYSQL {
		NET net;
		ubyte* connector_fd;
		char* host, user, passwd, unix_socket, server_version, host_info;
		char* info, db;
		void* charset; //charset_info_st?
		MYSQL_FIELD* fields;
		MEM_ROOT field_alloc;
		ulong affected_rows;
		ulong insert_id;
		ulong extra_info;
		size_t thread_id, packet_length;
		uint port;
		size_t client_flag, server_capabilities;
		uint protocol_version;
		uint field_count, server_status, server_language, warning_count;
		st_mysql_options options;
		mysql_status status;
		my_bool free_me;
		my_bool reconnect;
		char[SCRAMBLE_LENGTH + 1] scramble;
		my_bool unused1;
		void* unused2, unused3, unused4, unused5;
		void* stmts; // LIST
		void* methods; // st_mysql_methods;
		void* thd;
		my_bool* unbuffered_fetch_owner;
		char* info_buffer;
		void* extension;
	}

	
	extern(System) {
		MYSQL* mysql_init(MYSQL* mysql);
		MYSQL* mysql_real_connect(MYSQL* mysql, const char* host, const char* user, const char* passwd, const char *db, uint port, const char* unix_socket, size_t clientflag);
		int	mysql_select_db(MYSQL* mysql, const char* db);
		int	mysql_query(MYSQL* mysql, const char* q);
		const(char)* mysql_error(MYSQL *mysql);
		MYSQL_RES* mysql_store_result(MYSQL* mysql);
		MYSQL_RES* mysql_use_result(MYSQL* mysql);
		ulong mysql_num_rows(MYSQL_RES* res);
		uint mysql_num_fields(MYSQL_RES* res);
		uint mysql_field_count(MYSQL *mysql);
		MYSQL_FIELD* mysql_fetch_fields(MYSQL_RES* res);
		ulong mysql_affected_rows(MYSQL *mysql);
		MYSQL_ROW mysql_fetch_row(MYSQL_RES* result);
		uint* mysql_fetch_lengths(MYSQL_RES* result);
		void mysql_free_result(MYSQL_RES* result);
		void mysql_close(MYSQL* sock);
		MYSQL_STMT* mysql_stmt_init(MYSQL*mysql);
		int mysql_stmt_prepare(MYSQL_STMT* stmt, const char*query, size_t length);
		int mysql_stmt_execute(MYSQL_STMT* stmt);
		int mysql_stmt_fetch(MYSQL_STMT* stmt);
		int mysql_stmt_fetch_column(MYSQL_STMT* stmt, MYSQL_BIND*bind_arg, uint column, size_t offset);
		int mysql_stmt_store_result(MYSQL_STMT* stmt);
		ulong mysql_stmt_param_count(MYSQL_STMT* stmt);
		my_bool mysql_stmt_attr_set(MYSQL_STMT* stmt, enum_stmt_attr_type attr_type, const void* attr);
		my_bool mysql_stmt_attr_get(MYSQL_STMT* stmt, enum_stmt_attr_type attr_type, void* attr);
		my_bool mysql_stmt_bind_param(MYSQL_STMT* stmt, MYSQL_BIND* bnd);
		my_bool mysql_stmt_bind_result(MYSQL_STMT* stmt, MYSQL_BIND* bnd);
		my_bool mysql_stmt_close(MYSQL_STMT* stmt);
		my_bool mysql_stmt_reset(MYSQL_STMT* stmt);
		my_bool mysql_stmt_free_result(MYSQL_STMT* stmt);
		my_bool mysql_stmt_send_long_data(MYSQL_STMT* stmt, uint param_number, const char* data, size_t length);
		MYSQL_RES*mysql_stmt_result_metadata(MYSQL_STMT* stmt);
		MYSQL_RES*mysql_stmt_param_metadata(MYSQL_STMT* stmt);
		uint mysql_stmt_errno(MYSQL_STMT* stmt);
		const(char)* mysql_stmt_error(MYSQL_STMT* stmt);
		const(char)* mysql_stmt_sqlstate(MYSQL_STMT* stmt);
		MYSQL_ROW_OFFSET mysql_stmt_row_seek(MYSQL_STMT* stmt, MYSQL_ROW_OFFSET offset);
		MYSQL_ROW_OFFSET mysql_stmt_row_tell(MYSQL_STMT* stmt);
		void mysql_stmt_data_seek(MYSQL_STMT* stmt, ulong offset);
		ulong mysql_stmt_num_rows(MYSQL_STMT* stmt);
		ulong mysql_stmt_affected_rows(MYSQL_STMT* stmt);
		ulong mysql_stmt_insert_id(MYSQL_STMT* stmt);
		uint mysql_stmt_field_count(MYSQL_STMT* stmt);

		my_bool mysql_commit(MYSQL* mysql);
		my_bool mysql_rollback(MYSQL* mysql);
		my_bool mysql_autocommit(MYSQL* mysql, my_bool auto_mode);
		my_bool mysql_more_results(MYSQL* mysql);
		int mysql_next_result(MYSQL* mysql);
		int mysql_stmt_next_result(MYSQL_STMT* stmt);
		
		int mysql_options(MYSQL* mysql, mysql_option option, const void* arg);		
		my_bool mysql_ssl_set(MYSQL* mysql, const char* key, const char* cert, const char* ca, const char* capath, const char* cipher);
	}