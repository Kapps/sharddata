﻿module ShardData.MySql.MySqlConnectionInfo;

import ShardData.DbConnectionInfo;

/// Provides information about a MySql connection.
class MySqlConnectionInfo : DbConnectionInfo {

public:
	/// Initializes a new instance of the MySqlConnectionInfo object.
	this(string Username = null, string Password = null, string Host = null, string Database = null, ushort Port = 0) {
		super(Username, Password, Host, Database, Port);
		if(Port == 0)
			Port = 3306;
	}
	
private:
}