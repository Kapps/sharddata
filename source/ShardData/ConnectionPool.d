﻿module ShardData.ConnectionPool;
public import ShardData.IDbConnectionPool;
private import core.sync.mutex;
private import core.thread;
private import std.algorithm;
private import ShardTools.Stack;
private import ShardTools.ConcurrentStack;
private import ShardTools.Queue;
private import std.datetime;
private import core.atomic;
private import std.container;
private import ShardTools.LinkedList;
private import std.exception;
public import ShardData.DbConnection;


/// Provides a ConnectionPool that can be used in place of a DbConnection.
/// Unlike DbConnection, a ConnectionPool is entirely thread-safe, and dispatches query to any available connection.
/// Transactions are not supported on a ConnectionPool, unless manually acquiring and releasing a connection.
/// Because queries are dispatched to any available connection, this class effectively allows automatic parallelization of Sql queries.
class ConnectionPool(T : DbConnection) : T, IDbConnectionPool  {

public:

	// TODO: When executing a command on a pooled connection, detect if it fails.
	// Then, determine if the fail is because the connection was severed; if so, try a different connection and attempt to re-open that connection if it succeeds.
	// TODO: Switch back to a ConcurrentStack and make this class lock-free.

	/// Initializes a new instance of the ConnectionPool object.
	this(DbConnectionInfo ConnectionInfo) {
		super(ConnectionInfo);
		this.MaxConnections = 100;
		this.MinConnections = 1;
		this._OpenConnections = new OpenConnectionContainer();
		this._AvailableConnections = new AvailableConnectionContainer();
		this.SyncLock = new Mutex();
	}

	/// Gets or sets the minimum or maximum number of connections to be executing a command at once for this pool.
	/// Lowering the maximum connections will not close any existing connections until they are done their current query.
	/// For performance reasons, it is possible for the actual number of connections to be slightly higher than the maximum due to race conditios.
	@property size_t MaxConnections() const {
		return _MaxConnections;
	}

	/// Ditto
	@property void MaxConnections(size_t Value) {
		synchronized(SyncLock) {
			_MaxConnections = Value;
			ClearExtraConnections();
		}
	}

	/// Ditto
	@property size_t MinConnections() const {
		return _MinConnections;
	}

	/// Ditto
	@property void MinConnections(size_t Value) {
		synchronized(SyncLock) {
			enforce(Value <= MaxConnections, "MinConnections must be less than or equal to MaxConnections.");
			_MinConnections = Value;
		}
	}

	/// Gets the number of connections currently open in this pool.
	/// It is possible for this to be greater than MaxConnections when MaxConnections is lowered while queries are busy.	
	@property size_t OpenConnections() const {
		return _OpenConnections.length;
	}

	/// Gets the number of connections that are currently executing a command.
	/// It is possible for this to be greater than MaxConnections when MaxConnections is lowered while queries are busy.	
	@property size_t BusyConnections() const {		
		return _OpenConnections.length - _AvailableConnections.Count;		
	}

	/// Gets the number of connections that are available to be used.
	/// It is possible for this to be greater than MaxConnections when MaxConnections is lowered while queries are busy.
	@property size_t AvailableConnections() const {
		return _AvailableConnections.Count;
	}
	
	/// Gets the state of this connection.
	/// For a ConnectionPool, this method always returns Open.
	@property override DbConnectionState State() const {
		return DbConnectionState.Open;
	}

	/// Creates or re-uses an existing connection, returning the result.
	/// This method blocks until a connection is available, or until the Timeout value of the ConnectionPool is reached.	
	T AcquireConnection() {
		PooledConnection Result;
		if(_AvailableConnections.TryPop(Result))
			return Result;
		
		// TODO: IMPORTANT: When no timeout is specified, no other connections will be able to timeout.
		// Pretty big issue that needs fixing, but requires some form of queue of connections and tryLock instead of lock.
		SysTime Started = Clock.currTime();
		synchronized(SyncLock) {			
			while(_OpenConnections.length > MaxConnections) {
				if(this.Timeout != Duration.init && (Started + this.Timeout) > Clock.currTime())
					throw new TimeoutException("Unable to acquire a connection within the timeout duration.");
				Thread.sleep(dur!"msecs"(1));
			}
			if(_OpenConnections.length < MaxConnections) {
				Result = CreateConnection();
				_OpenConnections.insert(Result);
			}			
			MinAvailable = min(_AvailableConnections.Count, MinAvailable);
		}
		return Result;
	}

	/// Releases the given connection back in to the pool.
	/// The connection must have been returned by AcquireConnection, otherwise an error will be thrown.
	void ReleaseConnection(T Connection) {		
		// At the moment, this is an arbitrary restriction.
		enforce(cast(PooledConnection)Connection !is null, "The connection to release must have been created by this pool.");		
		enforce(Connection.ActiveTransaction is null);
		// Race condition here, but we consider it acceptable.
		if(OpenConnections >= MaxConnections) {
			(cast(PooledConnection)Connection).Sever();
			return;		
		}
		// TODO: Determine whether to push the connection by determining how close we came to minimum over the last X duration.		
		_AvailableConnections.Push(Connection);		
	}

	/+
	+ TODO: Create a class derived from DbAction that allows simply telling it when it's complete and provides a result.
	/// Creates or re-uses an existing connection.
	/// This method does not block.
	AsyncAction AcquireConnectionAsync() {

	}+/

protected:
	alias RedBlackTree!(PooledConnection, "a.ID < b.ID") OpenConnectionContainer;
	alias ConcurrentStack!(PooledConnection) AvailableConnectionContainer;

	OpenConnectionContainer _OpenConnections;
	AvailableConnectionContainer _AvailableConnections;	
	Mutex SyncLock;

	/// Creates a new connection using the ConnectionInfo used by this pool.	
	PooledConnection CreateConnection() {
		return new PooledConnection(this.ConnectionInfo);
	}

	class PooledConnection : T {
		size_t ID;
		private static __gshared size_t NextID;
		
		this(DbConnectionInfo CI) {
			super(CI);
			this.ID = atomicOp!("+=", size_t, int)(NextID, 1);			
		}

		override void Close() {
			ReleaseConnection(this);
		}

		/// Closes the pooled connection, instead of simply releasing it back in to the connection pool.
		void Sever() {
			super.Close();
		}
	}	
	
private:
	size_t _MinConnections;
	size_t _MaxConnections;

	/// The minimum number of connections that were available since the last over-allocation check.
	size_t MinAvailable;

	void ClearExtraConnections() {
		synchronized(SyncLock) {
			PooledConnection Current;
			while(_AvailableConnections.TryPop(Current) && OpenConnections > MaxConnections) {
				OpenConnections.remove(Current);
				Current.Sever();				
			}
		}
	}
}