﻿module ShardData.DbParameter;
private import std.exception;
private import std.socket;
private import ShardTools.ExceptionTools;
private import std.range;
private import std.typecons;
public import std.variant;
import core.memory;
import std.array;
import std.traits;
import std.c.string : memcpy;
import std.datetime;

//private alias std.socket.Address Address;
//private alias core.time.Duration Duration;


/// Determines the direction of a parameter.
enum ParameterDirection {
	/// The parameter is being passed in to a query.
	In,
	/// The parameter is returned from a query.
	Out,
	/// The parameter is passed in and possibly modified.
	InOut,
	/// The parameter is returned by a function.
	Return
}

/// Provides basic data types for most databases.
/// Implementors are free to extend this by setting the DbExtension bit.
enum ParameterType {
	/// Indicates no type or null.
	Null = 0,
	/// A (usually) one-byte boolean value; either true or false.
	Bool,
	/// Zero or more one-byte characters.
	Char,
	/// zero or more two-byte characters.
	WideChar,
	/// An array of bytes.
	Binary,
	/// A signed integer of N bits.
	Int8,
	/// Ditto	
	Int16,
	/// Ditto
	Int32,
	/// Ditto
	Int64,
	/// An unsigned integer of N bits.
	UInt8,
	/// Ditto
	UInt16,
	/// Ditto
	UInt32,
	/// Ditto
	UInt64,
	/// Provides a time and date with timezone information.
	SysTime,
	/// Provides a time and date without timezone information.
	DateTime,
	/// Indicates a period of time.
	Duration,
	/// A 32 bit floating point number.
	Single,
	/// A 64 bit floating point number.
	Double,
	/// An IPv4 or IPv6 address.
	InternetAddress,
	/// A narrow varchar.
	String,
	/// A wide varchar.
	WideString,
	/// When this bit is set, indicates that the type is none of the above types and instead a database-specific type.	
	/// To define a database specific type, declare it with a value of greater than DbExtension.
	DbExtension = 256
}

/// Represents a single parameter for a database command.
/// Parameters are only used for input values, not for returned values or selected columns.
abstract class DbParameter  {

public:
	/// Initializes a new instance of the DbParameter object for input purposes.
	this(string Name) {
		this._Name = Name;		
	}

	/// Gets the name of this parameter.
	/// This does not include any database-specific prefix or suffix (for example, this would be UserID instead of @UserID).
	@property string Name() const {
		return _Name;
	}

	/// Gets the name of this parameter, including any database-specific prefix or suffix (for example, this would be @UserID instead of UserID).
	@property abstract string FormattedName() const;

	/// Gets the type of this parameter.
	@property ParameterType Type() const {
		return _Type;
	}

	/// Gets or sets the direction of this parameter.
	@property ParameterDirection Direction() const {
		return _Direction;
	}

	/// Ditto
	@property void Direction(ParameterDirection Value) {
		_Direction = Value;
	}

	/// Gets or sets the value stored by this parameter.
	@property Variant Value() {
		return _Value;
	}

	/// Ditto
	@property void Value(Variant Val) {
		_Value = Val;
	}

	/+ /// Ditto
	@property void Value(T)(T Val) if(!is(T == Variant) && is(T == int)) {
		this.Value = Variant(Val);
	}+/

	/+ /// Gets the size, in bytes, that the current value for this parameter has (all elements).
	@property size_t SizeInBytes() const {
		return _SizeInBytes;
	}

	/// Gets the number of elements that	Value contains.
	@property size_t NumElements() const {
		return _NumElements;		
	}

	/// Gets the size of each individual element in Value, or zero if null.
	@property size_t ElementSize() const {
		return _NumElements == 0 ? 0 : _SizeInBytes / _NumElements;
	}

	/// Gets or sets the value that this parameter currently holds. Can be null.
	/// BUGS:
	/// 	Needs to be named Value but can't be because DMD decides it conflicts with the setter.
	@property void* GetValue() {
		return _Value;
	}

	
	/// Ditto
	@property void Value(T)(T Value) {
		static if(isArray!T)
			alias Unqual!(ElementEncodingType!(Unqual!(T))) ParamType;
		else
			alias Unqual!T ParamType;
		static assert(!isPointer!(T) && !isPointer!(ParamType));
		ParameterType Type;
		size_t Size;
		size_t NumElements;

		static if((is(ParamType == byte) || is(ParamType == ubyte)) && isArray!T)
			Type = ParameterType.Binary;
		else static if((is(ParamType == char) && isArray!T && !isStaticArray!T))
			Type = ParameterType.String;
		else static if(is(ParamType == byte))
			Type = ParameterType.Int8;			
		else static if(is(ParamType == ubyte))
			Type = ParameterType.UInt8;
		else static if(is(ParamType == short))
			Type = ParameterType.Int16;
		else static if(is(ParamType == ushort))
			Type = ParameterType.UInt16;
		else static if(is(ParamType == int))
			Type = ParameterType.Int32;
		else static if(is(ParamType == uint))
			Type = ParameterType.UInt32;
		else static if(is(ParamType == long))
			Type = ParameterType.Int64;
		else static if(is(ParamType == ulong))
			Type = ParameterType.UInt64;
		else static if(is(ParamType == DateTime))
			Type = ParameterType.DateTime;
		else static if(is(ParamType == SysTime))
			Type = ParameterType.SysTime;
		else static if(is(ParamType == Duration))
			Type = ParameterType.Duration;
		else static if(is(ParamType == char))
			Type = ParameterType.Char;
		else static if(is(ParamType == wchar))
			Type = ParameterType.WideChar;
		else static if(is(ParamType == float))
			Type = ParameterType.Single;
		else static if(is(ParamType == double))
			Type = ParameterType.Double;
		else static if(is(ParamType : Address))
			Type = ParameterType.InternetAddress;
		else static if(is(T == typeof(null))) // Is this safe?
			Type = ParameterType.Null;		
		else
			static assert(0, "Unable to infer type for value. Try using SetValue and specifying the type directly. Unqualified type was " ~ ParamType.stringof ~ ".");

		void* Ptr;
		static if(isArray!T) {
			Size = Value.length * ParamType.sizeof;
			NumElements = Value.length;
			Ptr = cast(void*)Value.ptr;
		} else {
			static if(!is(T == class) && !isPointer!T) {
				ubyte[] Copy = new ubyte[T.sizeof];
				memcpy(Copy.ptr, &Value, T.sizeof);
				Ptr = cast(void*)Copy.ptr;
			} else				
				Ptr = cast(void*)Value;			
			Size = ParamType.sizeof;
			NumElements = 1;
		}

		SetValue(Ptr, Type, Size, NumElements);
	}

	/// Sets this parameter to the given value, which the following type was calculated for.
	/// Params:
	/// 	Value = A pointer to the value to set, or null.
	/// 	Type = The type of the parameter that Value represents.
	/// 	SizeInBytes = The size, in bytes, of all elements pointed to by value.
	/// 	NumElements = The number of Elements Value represents. If Value is null, this is still one as it contains one null element.
	/// Storage:
	/// 	Most types are stored as a pointer to a copy of the initial value.
	/// 	Address however, is stored as an Address itself, and thus can be accessed by cast(Address)Value.
	/// 	As a result, SizeInBytes for an Address is always 1, and arrays of Addresses are not allowed.
	void SetValue(void* Value, ParameterType Type, size_t SizeInBytes, size_t NumElements) {
		if(_Type != ParameterType.Null && Type != _Type)
			throw new TypeChangeException("Unable to change the type of an already bound parameter.");
		if(Type == ParameterType.InternetAddress && NumElements > 0)
			enforce(NumElements == 1, "Arrays of Addresses are not yet supported.");		
		this._Value = Value;
		this._Type = Type;
		this._SizeInBytes = SizeInBytes;
		this._NumElements = NumElements;
	}+/

protected:	
	/+ /// Provides the raw data for a single element.
	alias ubyte[] ElementData;+/	
	
private:	
	//bool _IsNullable;
	string _Name;
	ParameterDirection _Direction;	
	//void* _Value;
	Variant _Value;
	ParameterType _Type;	
	//size_t _SizeInBytes;
	//size_t _NumElements;	
}