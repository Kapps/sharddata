﻿module ShardData.DbCachePolicy;
private import std.exception;
private import ShardData.DbCommand;
public import ShardData.DbDataSet;

/// Provides the caching policy for a DbCommand.
class DbCachePolicy  {

public:
	/// Initializes a new instance of the DbCachePolicy object.
	this() {
		
	}

	//mixin(Mixins.CreateLowercaseAliases!(typeof(this)));

	/// Invalidates the cached result set, forcing a new one to be assigned.
	void Invalidate() {		
		synchronized(this) {
			if(this._ResultSet !is null && this._ResultSet.IsOpen) {
				this._ResultSet._Cache = null;
				this._ResultSet.Close();
			}
			this._ResultSet = null;		
		}
	}

	/// Gets the result set, provided that the cached results have not expired or been invalidated.
	/// If no result set is cached, or the result set is expired, null is returned.
	@property DbDataSet ResultSet() {		
		synchronized(this) {
			if(_ResultSet !is null && !_ResultSet.IsOpen) {
				Invalidate();
				return null;
			}			
			return _ResultSet;		
		}
	}

	/// Assigns the given result set to be cached according to this cache policy.
	@property void ResultSet(DbDataSet Set) {		
		synchronized(this) {
			Invalidate();		
			if(Set) {
				enforce(Set._Cache is null);			
				Set._Cache = this;
			}
			this._ResultSet = Set;					
		}
	}

protected:
	/// The result set being cached.
	DbDataSet _ResultSet;	
}