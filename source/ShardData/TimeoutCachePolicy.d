﻿module ShardData.TimeoutCachePolicy;
private import std.datetime;

import ShardData.DbCachePolicy;

/// Provides a DbCachePolicy that caches results for a given duration.
class TimeoutCachePolicy : DbCachePolicy {

public:
	/// Initializes a new instance of the TimeoutCachePolicy object.
	this(Duration CacheDuration) {
		this._CacheDuration = CacheDuration;
	}

	/// Gets or sets the amount of time to cache results for.
	/// Changes to this value take effect upon the next cache request.
	@property Duration CacheDuration() const {
		return _CacheDuration;
	}

	/// Ditto
	@property void CacheDuration(Duration Value) {
		this._CacheDuration = Value;
	}

	/// Indicates whether this result set has expired without having a new result set assigned.
	@property bool HasExpired() const {
		return (LastCache + _CacheDuration) <= Clock.currTime();
	}

	/// Gets the result set, provided that the cached results have not expired or been invalidated.
	/// If no result set is cached, or the result set is expired, null is returned.
	@property override DbDataSet ResultSet() {
		synchronized(this) {
			if(HasExpired)
				Invalidate();
			return super.ResultSet;
		}
	}

	/// Ditto
	@property override void ResultSet(DbDataSet Set) {
		synchronized(this) {
			super.ResultSet = Set;
			this.LastCache = Clock.currTime();
		}
	}

protected:
	
private:
	SysTime LastCache;
	Duration _CacheDuration;	
}