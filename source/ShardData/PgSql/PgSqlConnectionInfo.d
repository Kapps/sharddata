﻿module ShardData.PgSql.PgSqlConnectionInfo;
public import ShardData.DbConnectionInfo;


/// Provides connection info to a Postgres database.
class PgSqlConnectionInfo : DbConnectionInfo {

public:
	/// Initializes a new instance of the PgSqlConnectionInfo object.
	this(string Username = null, string Password = null, string Host = null, string Database = null, ushort Port = 0) {
		super(Username, Password, Host, Database, Port);		
	}
	
private:
}