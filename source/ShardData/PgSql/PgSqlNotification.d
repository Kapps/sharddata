﻿module ShardData.PgSql.PgSqlNotification;
private import ShardTools.ArrayOps;
private import std.algorithm;
private import std.exception;
private import std.array;
public import ShardData.PgSql.PgSqlConnection;
private import std.conv;

public import ShardData.DbNotification;

/// Provides an implementation of DbNotification for postgresql.
class PgSqlNotification : DbNotification {

public:
	/// Initializes a new instance of the PgSqlNotification object.
	this(string Queue, DbConnection Connection) {
		enforce(!Contains(Queue, '\"'), "Illegal character in queue name.");
		super(Queue, Connection);
	}

package:

	void SignalNotificationReceived(uint pid, string payload) {
		this.NotifyNotificationReceived(payload, ["pid" : to!string(pid)]);
	}

protected:

	/// Override to begin listening for notifications.
	/// When a notification is received, the NotifyNotificationReceived method should be called.
	override void PerformListen() {		
		Connection.QueryAsync("LISTEN \"" ~ Queue ~ "\"");
		PgSqlConnection Conn = cast(PgSqlConnection)this.Connection;
		Conn.RegisterNotification(this);
	}

	/// Override to stop listening for notifications.
	override void PerformUnlisten() {		
		Connection.QueryAsync("UNLISTEN \"" ~ Queue ~ "\"");
	}

	/// Override to send a notification to this queue, with the given additional information or payload.
	override void PerformSendNotification(string Payload) {		
		//Payload = replace(Payload, "\'", "\'\'");
		Connection.QueryAsync("SELECT pg_notify($1, $2)", Queue, Payload);
		//Connection.QueryAsync("NOTIFY \"" ~ Queue ~ "\", \'" ~ Payload ~ "\'");
	}
	
private:
}