﻿module ShardData.PgSql.PgSqlCommand;
private import ShardTools.Queue;
private import ShardTools.ConcurrentStack;
private import ShardData.PgSql.PgSqlConversions;
private import core.atomic;
private import core.sync.mutex;
private import std.stdio;
private import std.datetime;
private import std.socket;
public import ShardData.PgSql.PgSqlDataSet;
private import std.array;
private import std.string;
private import std.conv;
public import ShardData.PgSql.PgSqlParameter;
public import ShardData.PgSql.PgSqlConnection;
private import std.exception;
import ShardData.PgSql.PgSqlDefines;
import ShardTools.BitOps;

alias void delegate(Variant, out DataFormat, out ubyte[]) PgSqlTypeSenderDelegate;

/// Provides a single prepared query for a Postgres database.
/// Note that only input parameters are required; the command itself will generate the non-provided output parameters.
/// BUGS:
///		Executing the command for the first time requires it to be prepared, which should be asynchronous but is not yet.
class PgSqlCommand : DbCommand {

	// TODO: Force not being able to change Oid when a command is prepared.

public:

	//mixin(Mixins.CreateLowercaseAliases!(typeof(this)));

	/// Initializes a new instance of the PgSqlCommand object.
	this(PgSqlConnection Connection, string CommandText = null) {
		super(Connection, CommandText);				
		IsPrepared = false;
		this.QueuedSends = new typeof(QueuedSends)();
	}

	this(PgSqlConnection Connection, string CommandText, PgSqlParameter[] Parameters...) {
		this(Connection, CommandText);
		foreach(PgSqlParameter Param; Parameters) {
			AddParameter(Param);
		}
	}

	/// Gets or sets the command text to execute.
	@property override void CommandText(string Text) {
		synchronized(this) {
			if(IsPrepared || WaitingForPrepare)
				throw new CommandPreparedException();
			super.CommandText(Text);
		}
	}

	/// Ditto
	@property override string CommandText() {
		return super.CommandText;
	}

	/// Gets the connection being used for this command.
	@property override PgSqlConnection Connection() {
		return cast(PgSqlConnection)super.Connection();
	}

	/// Gets the PSQL prepared statement name for this command.
	@property string Name() const {
		return _Name;
	}

	/// Adds the given parameter to the command for execution.
	/// Params:
	/// 	Parameter = The parameter to add to the command.
	override void AddParameter(DbParameter Parameter) {
		PgSqlParameter msqParam = cast(PgSqlParameter)Parameter;
		enforce(msqParam);
		super.AddParameter(Parameter);		
	}
	
protected:

	void EnsurePrepareQueued() {
		Oid[] IDs = null;
		char* CommandText;
		synchronized(this) {
			if(IsPrepared || WaitingForPrepare)
				return;					
			WaitingForPrepare = true;
			static shared size_t NextNumber;
			size_t CommandNumber = atomicOp!"+="(NextNumber, 1);
			this._Name = "SD" ~ to!string(CommandNumber) ~ '\0';
			this._Name = this._Name[0..$-1]; // Force null terminator.
			CommandText = cast(char*)toStringz(this.CommandText);
			IDs = new Oid[Parameters.length];			
			foreach(Index, Parameter; Parameters) {
				IDs[Index] = (cast(PgSqlParameter)Parameter).GetOid();
			}
		}				
		PGconn* conn = Connection.Connection;			
			
		int SendPrepare() {				
			return PQsendPrepare(conn, _Name.ptr, CommandText, cast(int)IDs.length, IDs.ptr);				
		}

		int SendDescribe() {
			return PQsendDescribePrepared(conn, _Name.ptr);
		}

		PgSqlDataSet ReceiveDescribed(PGresult* res) {
			synchronized(this) {
				int NumFields = PQnfields(res);
				for(size_t i = 0; i < NumFields; i++) {
					const(char*) Name = PQfname(res, cast(int)i);
					if(Name is null)
						throw new CommandFailedException("Unable to get the name of a field in the command.");
					string NameString = to!string(Name);
					FieldMap[NameString] = i;
				}
				IsPrepared = true;
				WaitingForPrepare = false;
				while(QueuedSends.Count > 0) {
					QueuedSend Send = QueuedSends.Dequeue();
					Connection.QueueCommand(Send.Action, Send.SendMethod, Send.CompletionCallback);
				}
				QueuedSends = null;
			}
			return null;
		}	
		
		PgSqlDataSet ReceivePrepareResult(PGresult* res) {
			auto Action = CreateAction();
			Connection.QueueCommand(Action, &SendDescribe, &ReceiveDescribed);			
			return null;
		}		

		auto PrepareAct = CreateAction();
		Connection.QueueCommand(PrepareAct, &SendPrepare, &ReceivePrepareResult);
	}	

	~this() {
		if(PrepareResult)
			PQclear(PrepareResult);
	}

protected:
	
	/// Override to handle executing the command and returning a DataSet.
	override DbAction PerformExecuteAsync() {
		//PGresult* res = ExecuteCommonAsync();
		//return new PgSqlDataSet(Connection, FieldMap, res);

		// TODO: Do we need to reprepare if the connection gets reset?	
		synchronized(this) {	
			EnsurePrepareQueued();			
				
			size_t ParamCount = this.Parameters.length;
			// TODO: Can easily optimize to use a buffer, and eliminate below closure as well. But not a big deal...
			int[] Lengths = uninitializedArray!(int[])(ParamCount);
			int[] Formats = uninitializedArray!(int[])(ParamCount);		
			char*[] Values = uninitializedArray!(char*[])(ParamCount);					
			PgSqlConnection PgConn = cast(PgSqlConnection)Connection;
			PGconn* lpConn = PgConn.Connection;			
			for(size_t i = 0; i < ParamCount; i++) {
				PgSqlParameter Param = cast(PgSqlParameter)Parameters[i];
				assert(Param);
				SetParameter(Param, &Values[i], &Lengths[i], &Formats[i]);
			}
			//PGresult* result = PQexecPrepared(lpConn, toStringz(_Name), ParamCount, Values.ptr, Lengths.ptr, Formats.ptr, FORMAT_BINARY);
			//CheckResult(result, "executing the query");		
			DbAction Act = CreateAction();
			int SendCommand() {				
				return PQsendQueryPrepared(lpConn, _Name.ptr, cast(int)ParamCount, Values.ptr, Lengths.ptr, Formats.ptr, cast(int)DataFormat.Text);					
			}			
			if(WaitingForPrepare || !IsPrepared) {
				QueuedSends.Enqueue(QueuedSend(Act, &SendCommand, &OnCommandComplete));				
			} else {
				PgConn.QueueCommand(Act, &SendCommand, &OnCommandComplete);
			}
			return Act;
		}		
	}

	private DbAction CreateAction() {
		DbAction Act = new DbAction(this.Connection);
		return Act;
	}

	private PgSqlDataSet OnCommandComplete(PGresult* res) {		
		PgSqlDataSet Set = new PgSqlDataSet(this.Connection, FieldMap, res);
		return Set;
	}

	/// Creates a parameter with the given name.
	override DbParameter CreateParameter(string Name) {
		return new PgSqlParameter(Name);
	}

private:
	//Guaranteed to be null terminated for the sake of preventing requiring toStringz everywhere.
	string _Name;
	bool IsPrepared;
	bool WaitingForPrepare;
	package size_t[string] FieldMap;
	PGresult* PrepareResult;	
	
	alias Tuple!(DbAction, "Action", SendCommandDelegate, "SendMethod", CompletionPointer, "CompletionCallback") QueuedSend;
	Queue!(QueuedSend, false) QueuedSends;

	private void SetParameter(PgSqlParameter Param, char** Value, int* Length, int* Format) {		
		ubyte[] Data = PgSqlConversions.ConvertToCommand(Param.Value, *cast(DataFormat*)Format);
		if(*Format == cast(int)DataFormat.Binary)
			SwapEndianOrder(Data);
		*Length = cast(int)Data.length;
		*Value = cast(char*)Data.ptr;
	}
	 
	/// For internal use; supposed to be package but package dislikes the idea of working properly.
	package static void CheckResult(PGresult* res, PgSqlConnection Connection, lazy string Action) {
		PGconn* conn = Connection.Connection;
		synchronized(Connection.ConnectionLock) {
			if(res is null) { // TODO: Raise a Condition instead.
				string Details = to!string(PQerrorMessage(conn));
				throw new CommandFailedException("Error while " ~ Action() ~ ", probably due to a connection error or due to being out of memory. " ~ Details);
			}
			auto Status = PQresultStatus(res);
			if(Status == ExecStatusType.PGRES_BAD_RESPONSE || Status == ExecStatusType.PGRES_FATAL_ERROR || Status == ExecStatusType.PGRES_NONFATAL_ERROR) {
				string ErrorMessage = to!string(PQresultErrorMessage(res));
				PQclear(res);
				throw new CommandFailedException("An error occurred while " ~ Action() ~ ". " ~ ErrorMessage);
			}
		}
	}
}