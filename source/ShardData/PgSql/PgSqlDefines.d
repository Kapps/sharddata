﻿module ShardData.PgSql.PgSqlDefines;


extern(C) {

	struct PGconn { }
	struct PGresult { }

	struct PGnotify {
		char* relname;
		int be_pid;
		char* extra;
	}

	enum ConnStatusType : int {
		CONNECTION_OK,
		CONNECTION_BAD,
		CONNECTION_STARTED,
		CONNECTION_MADE,
		CONNECTION_AWAITING_RESPONSE,
		CONNECTION_AUTH_OK,
		CONNECTION_SETENV,
		CONNECTION_SSL_STARTUP,
		CONNECTION_NEEDED
	}

	enum ExecStatusType : int {
		PGRES_EMPTY_QUERY,
		PGRES_COMMAND_OK,
		PGRES_TUPLES_OK,
		PGRES_COPY_OUT,
		PGRES_COPY_IN,
		PGRES_BAD_RESPONSE,
		PGRES_NONFATAL_ERROR,
		PGRES_FATAL_ERROR
	}

	alias int Oid;

	enum PgOids {
		Bool = 16,		
		Char = 18,
		Int64 = 20,
		Int16 = 21,
		Int32 = 23,
		Text = 25,
		Oid = 26,
		Xml = 142,
		Single = 700,
		Double = 701,		
		SysTime = 1114,
		Duration = 703,
		Unknown = 705,
		IP = 869,
		//CharArray = 1042,
		VarChar = 1043,
		DateTime = 702,
		Binary = 17
	}

	PGconn* PQconnectdb(const char* conninfo);
	PGresult* PQexec(PGconn* conn, const char* query);
	ConnStatusType PQstatus(const PGconn *conn);
	ExecStatusType PQresultStatus(const PGresult* res);
	char* PQerrorMessage(PGconn* conn);
	char* PQresultErrorMessage(const PGresult* res);
	int PQntuples(const PGresult* res); // TODO: Is this right? The header says it is, the docs say it is, but docs also say it may overflow on 32-bit systems...
	void PQfinish(PGconn *conn);
	PGresult* PQdescribePrepared(PGconn* conn, const char* stmtName);
	int PQsendDescribePrepared(PGconn* conn, const char* stmtName);
	void PQclear(PGresult* res);
	int PQnfields(const PGresult* res);
	const(char*) PQfname(PGresult* res, int index);
	PGresult* PQprepare(PGconn* conn, const char* stmtName, const char* query, int nParams, const Oid* paramTypes);
	int PQsendPrepare(PGconn* conn, const char* stmtName, const char* query, int nParams, const Oid* paramTypes);
	PGresult* PQexecPrepared(PGconn* conn, const char* stmtName, int nParams, const char** paramValues, const int* paramLengths, const int* paramFormats, int resultFormat);
	int PQgetlength(const PGresult* res, int row_number, int column_number);
	char* PQgetvalue(const PGresult* res, int row_number, int column_number);
	int PQisBusy(PGconn* conn);
	int PQconsumeInput(PGconn* conn);
	PGresult* PQgetResult(PGconn* conn);
	//int PQsendQueryParams(PGconn *conn, const char* command, int nParams, const Oid* paramTypes, const char** paramValues, const int* paramLengths, const int* paramFormats, int resultFormat);
	int PQsendQueryPrepared(PGconn* conn, const char* stmtName, int nParams, const char** paramValues, const int* paramLengths, const int* paramFormats, int resultFormat);
	int PQsendQueryParams(PGconn* conn, const char* command, int nParams, const Oid* paramTypes, const char** paramValues, const int* paramLengths, const int* paramFormats, int resultFormat);
	PGnotify* PQnotifies(PGconn* conn);
	void PQfreemem(void* ptr);
	int PQisthreadsafe();
	int PQputCopyData(PGconn* conn, const char* buffer, int nbytes);
	int PQputCopyEnd(PGconn* conn, const char* errormsg);
	Oid PQftype(PGresult* res, int column_number);
}