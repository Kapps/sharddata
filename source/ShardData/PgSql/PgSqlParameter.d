﻿module ShardData.PgSql.PgSqlParameter;
private import ShardTools.ExceptionTools;
private import ShardData.PgSql.PgSqlDefines;
public import ShardData.DbParameter;

mixin(MakeException("UnsupportedParameterException", "A parameter type passed in was not supported. To force a specific parameter, set the ParameterType to DbExtension and specify the Oid + DbExtension as the value."));

/// Provides a single parameter in a PSQL query.
class PgSqlParameter : DbParameter {

public:
	/// Initializes a new instance of the PgSqlParameter object.
	this(string Name) {
		super(Name);
	}	

	/// Gets the name of this parameter, including any database-specific prefix or suffix (for example, this would be @UserID instead of UserID).
	@property override string FormattedName() const {
		return '@' ~ Name;
	}

	/// Gets the Oid of the value contained by this command.
	@property Oid GetOid() {
		if(Type > cast(int)ParameterType.DbExtension)
			return cast(Oid)(Type - ParameterType.DbExtension);		
		switch(this.Type) {
			case ParameterType.Binary:
				return PgOids.Binary;
			case ParameterType.Bool:
				return PgOids.Bool;
			case ParameterType.Char:
				return PgOids.Char;
			case ParameterType.DateTime:
				return PgOids.DateTime;
			case ParameterType.Double:
				return PgOids.Double;
			case ParameterType.Duration:
				return PgOids.Duration;
			case ParameterType.Int16:
			case ParameterType.UInt16:
				return PgOids.Int16;
			case ParameterType.Int32:
			case ParameterType.UInt32:
				return PgOids.Int32;
			case ParameterType.Int64:
			case ParameterType.UInt64:
				return PgOids.Int64;
			case ParameterType.Int8:
			case ParameterType.UInt8:
				return PgOids.Char;
			case ParameterType.Null:
				return PgOids.Unknown;
			case ParameterType.Single:
				return PgOids.Single;
			case ParameterType.SysTime:
				return PgOids.SysTime;
			case ParameterType.InternetAddress:				
				return PgOids.IP;
			case ParameterType.String:
				return PgOids.VarChar;
			default:
				throw new UnsupportedParameterException();
		}				
	}
	
private:
	size_t _NumElements;
}