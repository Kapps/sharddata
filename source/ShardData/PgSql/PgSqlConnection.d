﻿module ShardData.PgSql.PgSqlConnection;
public import ShardData.PgSql.PgSqlQueryGenerator;
private import ShardTools.Queue;
private import ShardTools.TaskRepeater;
private import core.sync.mutex;
public import ShardData.PgSql.PgSqlNotification;
private import ShardTools.BufferPool;
private import ShardTools.Buffer;
private import std.stdio;
private import std.parallelism;
private import std.exception;
private import core.thread;
private import ShardTools.LinkedList;
private import ShardTools.Stack;
private import ShardTools.ConcurrentStack;
public import ShardData.PgSql.PgSqlCommand;
public import ShardData.PgSql.PgSqlTransaction;
private import std.string;
private import std.conv;
private import ShardData.PgSql.PgSqlDefines;
public import ShardData.DbConnection;
public import ShardData.PgSql.PgSqlConnectionInfo;
import ShardTools.ExceptionTools;
import ShardData.DbCommand;
import std.c.stdlib;
import std.c.string;
import ShardTools.Untyped;

/// Provides a connection to a Postgres database.
class PgSqlConnection : DbConnection {

public:
	/// Initializes a new instance of the PgSqlConnection object.
	this(DbConnectionInfo ConnectionInfo) {
		super(ConnectionInfo);
		QueuedCommands = new Queue!(QueuedCommand*)();
		this.QueueLock = new Mutex();
		this.NotificationLock = new Mutex();
		this.ConnectionLock = new Mutex();
		this._QueryGen = new PgSqlQueryGenerator();
	}

	/// Returns a query generator instance used to generate queries for this particular database.
	@property override PgSqlQueryGenerator QueryGenerator() {
		return _QueryGen;
	}

	/// Performs a copy operation for the given data. See the COPY command for more details.
	/// Params:
	/// 	Data = The data for the copy operation. In native postgres format.
	/// 	Query = The query to start the copy operation. In native postgres format.
	DbAction PerformCopy(string Query, string Data) {
		// TODO: This needs to be significantly improved.
		// For one, the point of Copy is large data sets. So why force a string that gets forced to be in memory...
		// Instead, use a delegate and return whether more rows should be sent.
		// For another, the whole being forced to figure out the format yourself. Instead there should be a helper class.
		// TODO: Move this.
		// It should be in it's own class, a copy helper. Then it should handle it's own sending as well.
		// It could also just use a StreamInput, which is a good choice.
		DbAction Act = new DbAction(this);
		
		int SendMethod() {			
			PGresult* res = PQexec(Connection, toStringz(Query));
			PgSqlCommand.CheckResult(res, this, "starting the copy operation");
			PQclear(res);
			int putRes = PQputCopyData(Connection, Data.ptr, cast(int)Data.length);
			if(putRes != 1) {
				string errorMsg = to!string(PQerrorMessage(Connection));
				PQputCopyEnd(Connection, toStringz("Failed to transmit data: " ~ errorMsg));
			} else {
				int endRes = PQputCopyEnd(Connection, null);
				if(endRes != 1) {
					string errorMsg = to!string(PQerrorMessage(Connection));
					throw new CommandFailedException("The copy operation failed: " ~ errorMsg);
				}
			}
			return 1;
		}

		PgSqlDataSet OnComplete(PGresult* res) {
			synchronized(ConnectionLock) {
				return new PgSqlDataSet(this, null, res);
			}
		}

		QueueCommand(Act, &SendMethod, &OnComplete);
		return Act;
	}

protected:

	private struct QueryAllocations {
		char* Query;
		char** ParamData;
		size_t ParamLength;
	}

	/// Override to execute a raw query.
	override DbAction PerformQueryAsync(string Query, string[] Params) {
		// TODO: Remove the code duplication that exists here and with PgSqlCommand.
		// Then make PgSqlCommand use malloc instead of the GC.
		// Maybe. The fact that it uses variant in a result set makes it pretty pointless.
		//		Not anymore!
		QueryAllocations* Allocations = cast(QueryAllocations*)malloc(QueryAllocations.sizeof);		
		auto lpQuery = cast(char*)malloc(Query.length + 1);
		memcpy(lpQuery, Query.ptr, Query.length);
		*(lpQuery + Query.length) = '\0';
		Allocations.Query = lpQuery;

		DbAction Act = new DbAction(this);
		char** ValuePtrs = cast(char**)malloc(size_t.sizeof * Params.length);
		foreach(Index, ref Param; Params) {
			char* ValuePtr = cast(char*)malloc(Param.length + 1);
			memcpy(ValuePtr, Param.ptr, Param.length);
			*(ValuePtr + Param.length) = '\0';
			ValuePtrs[Index] = ValuePtr;			
		}
		Allocations.ParamData = ValuePtrs;
		Allocations.ParamLength = Params.length;

		Act.NotifyOnComplete(Untyped(Allocations), &OnRawQueryComplete);		
		int SendQuery() {			
			return PQsendQueryParams(Connection, cast(const)lpQuery, cast(int)Params.length, null, cast(const)ValuePtrs, null, null, cast(int)DataFormat.Text);
		}

		PgSqlDataSet OnResultReceived(PGresult* res) {
			//BufferPool.Release(buff, true);
			
			size_t[string] FieldMap;
			int NumFields = PQnfields(res);

			for(size_t i = 0; i < NumFields; i++) {
				const(char*) Name = PQfname(res, cast(int)i);
				if(Name is null)
					throw new CommandFailedException("Unable to get the name of a field in the command.");
				string NameString = to!string(Name);
				FieldMap[NameString] = i;
			}

			PgSqlDataSet Result = new PgSqlDataSet(this, FieldMap, res);
			return Result;
		}
		
		this.QueueCommand(Act, &SendQuery, &OnResultReceived);		
		return Act;
	}

	private void OnRawQueryComplete(Untyped State, AsyncAction Action, CompletionType Type) {
		QueryAllocations* allocs = cast(QueryAllocations*)State;
		free(allocs.Query);
		for(size_t i = 0; i < allocs.ParamLength; i++)
			free(allocs.ParamData[i]);
		free(allocs.ParamData);
		free(allocs);		
	}

	/// Override to perform the actual opening of the connection.
	override void PerformOpen() {
		synchronized(this.ConnectionLock, this) {
			string OpenQuery = "";
			PgSqlConnectionInfo ci = cast(PgSqlConnectionInfo)ConnectionInfo;
			if(ci.Host)
				OpenQuery ~= "host = \'" ~ EscapeCI(ci.Host) ~ "\'";
			if(ci.Port)
				OpenQuery ~= " port = \'" ~ EscapeCI(ci.Port) ~ "\'";
			if(ci.Username)
				OpenQuery ~= " user = \'" ~ EscapeCI(ci.Username) ~ "\'";
			if(ci.Password)
				OpenQuery ~= " password = \'" ~ EscapeCI(ci.Password) ~ "\'";
			if(ci.Database)
				OpenQuery ~= " dbname = \'" ~ EscapeCI(ci.Database) ~ "\'";
			OpenQuery ~= " sslmode = \'";
			final switch(ci.SSL) {			
				case SslMode.Never:
					OpenQuery ~= "disable";
					break;
				case SslMode.Prefer:
					OpenQuery ~= "prefer";
					break;
				case SslMode.PreferNot:
					OpenQuery ~= "allow";
					break;
				case SslMode.Require:
					OpenQuery ~= "require";
					break;				
			}		
			OpenQuery ~= "\'";
			// No compression supported.
			PGconn* conn = PQconnectdb(toStringz(OpenQuery));
			ConnStatusType Status = PQstatus(conn);
			if(Status != ConnStatusType.CONNECTION_OK)
				throw new ConnectionFailedException("Connecting to the database failed. " ~ to!string(PQerrorMessage(conn)));		
			this.Connection = conn;
		}
	}

	override void PerformClose() {
		while(true) {
			synchronized(this) {
				if(QueuedCommands.Count > 0) {
					Thread.sleep(dur!"msecs"(1));
					continue;
				}
			}
			synchronized(this.ConnectionLock) {
				PQfinish(Connection);
			}
		}				
	}

	/// Override to create a new Transaction on this connection.
	override DbTransaction CreateTransaction() {		
		// TODO: Make this async.
		return new PgSqlTransaction(this);
	}
		
	/// Override to create a new command on this connection.	
	override DbCommand PerformCreateCommand() {
		return new PgSqlCommand(this);
	}

package:	

	/// Queues a DbAction to be executed asynchronously.
	/// Params:
	/// 	Action = The action being executed.
	/// 	ExecuteMethod = A callback to invoke to execute the command.	
	/// 	CompletionCallback = A callback to invoke when the command is executed.
	void QueueCommand(DbAction Action, SendCommandDelegate ExecuteMethod, CompletionPointer CompletionCallback) {
		QueuedCommand* Comm = new QueuedCommand(Action, ExecuteMethod, CompletionCallback);		
		synchronized(QueueLock) {
			//debug writeln("Queued command.");
			QueuedCommands.Enqueue(Comm);			
			//Action.NotifyOnComplete(cast(void*)Action, &OnActionComplete);
			if(!IsQueueRepeaterRunning) {
				IsQueueRepeaterRunning = true;
				//debug writeln("Starting poller.");
				TaskRepeater.Default.AddTask(&PollCommandCompletion);				
			}
		}
	}

	void RegisterNotification(PgSqlNotification Notification) {
		string Queue = Notification.Queue;
		synchronized(NotificationLock) {
			NotifierCollection* Coll = (Notification.Queue in Notifications);
			if(Coll)
				*Coll ~= Notification;					
			else
				Notifications[Notification.Queue] = [Notification];
			NotificationCount++;
			if(!IsNotifyRepeaterRunning) {
				IsNotifyRepeaterRunning = true;
				TaskRepeater.Default.AddTask(&PollNotifications);
			}
		}
	} 
	
private:
	package PGconn* Connection;	
	package Mutex ConnectionLock;

	Mutex QueueLock;
	bool IsQueueRepeaterRunning = false;	
	bool WaitingForQueueResult = false;
	QueuedCommand* Current = null;
	Queue!(QueuedCommand*) QueuedCommands;	

	Mutex NotificationLock;
	alias PgSqlNotification[] NotifierCollection;
	size_t NotificationCount;
	bool IsNotifyRepeaterRunning = false;
	NotifierCollection[string] Notifications;	

	PgSqlQueryGenerator _QueryGen;


	string EscapeCI(T)(T Data) {
		return translate(to!string(Data), ['\\':"\\\\", '\'':"\\\'"]);
	}

	RepeaterFlags PollNotifications() {
		synchronized(ConnectionLock) {
			int ConsumeRes = PQconsumeInput(Connection);
		}
		// TODO: A Condition raised when this fails.
		synchronized(NotificationLock) {
			if(NotificationCount == 0) {
				IsNotifyRepeaterRunning = false;
				return RepeaterFlags.None;
			}
		}

		PGnotify* notify;
		synchronized(ConnectionLock) {
			notify = PQnotifies(Connection);
		}
		if(notify) {
			string Queue = to!string(notify.relname);
			string Payload = to!string(notify.extra);
			int pid = notify.be_pid;
			PQfreemem(notify);
			synchronized(NotificationLock) {
				NotifierCollection* Coll = (Queue in Notifications); //Notifications.get(Queue, null);
				if(Coll) {				
					foreach(PgSqlNotification Notification; *Coll) {
						Notification.SignalNotificationReceived(pid, Payload);
					}
				}
			}
			return RepeaterFlags.WorkDone | RepeaterFlags.Continue;
		}	
		return RepeaterFlags.Continue;
	}

	RepeaterFlags PollCommandCompletion() {		
		synchronized(QueueLock) {
			if(QueuedCommands.Count == 0 && Current is null) {
				IsQueueRepeaterRunning = false;
				//debug writeln("Stopping poller.");				
				return RepeaterFlags.None; 
			}									 
			if(Current is null && !WaitingForQueueResult) {
				//debug writeln("Popped command.");
				Current = QueuedCommands.Dequeue();
				try {
					//debug writeln("Executed command.");
					int SendResult;
					synchronized(ConnectionLock) {
						SendResult = Current.ExecuteMethod();
					}
					if(SendResult == 0)
						throw new CommandFailedException("Unable to send message to database server: " ~ to!string(PQerrorMessage(Connection)));
				} catch (Throwable t) {
					// When the execute fails, skip this command.		
					debug writeln(t);			
					Current.Action.NotifyFailure(t);
					Current = null;					
				}
			}
		}
		// Check if we have data available.		
		synchronized(ConnectionLock) {
			// Have libpq read any data from the server.
			int ConsumeRes = PQconsumeInput(Connection);
			// We can't really handle a failed input read... usually implies a connection error I believe.
			/+if(ConsumeRes == 0) {
				// In case of failure, remove the command from the queue and notify it that it failed to execute.
				DbAction Action = Command.Action;
				string Details = to!string(PQerrorMessage(Connection));
				Throwable ex = new CommandFailedException("Error while getting the results of a command: " ~ Details);							
				synchronized(QueueLock)
					QueuedCommands.pop();
				Action.NotifyFailure(ex);			
				return RepeaterFlags.Continue;
			}+/
		}
		bool IsReady;
		synchronized(ConnectionLock) {	
			IsReady = (PQisBusy(Connection) == 0);
		}
		if(IsReady && !WaitingForQueueResult && Current !is null) {
			WaitingForQueueResult = true;				
			// If so, dispatch a task to send the result.
			//debug writeln("Queueing result.");
			taskPool.put(task(&SendResult, Current));
			Current = null;				
			return RepeaterFlags.WorkDone | RepeaterFlags.Continue;		
		}
		return RepeaterFlags.Continue;
	}

	private void SendResult(QueuedCommand* Comm) {		
		bool Notified = false;	
		try {					
			// We only care about the last result from a command.
			//debug writeln("Getting result.");			
			PGresult* Result = null;
			PgSqlDataSet Set = null;
			synchronized(ConnectionLock) {
				while(true) {
					PGresult* nextRes = PQgetResult(Connection);
					if(nextRes !is null) {
						Result = nextRes;
						PgSqlCommand.CheckResult(Result, this, "getting the results of the command");
					} else
						break;
				}
				PgSqlCommand.CheckResult(Result, this, "getting the results of the command");
				Set = Comm.CompletionMethod(Result);				
			}
			synchronized(QueueLock) {
				WaitingForQueueResult = false;
			}			
			Notified = true;
			Comm.Action.NotifyResult(Set);			
		} catch (Throwable thrown) {			
			if(!Notified) {
				synchronized(QueueLock)
					WaitingForQueueResult = false;
				Comm.Action.NotifyFailure(thrown);
			}
		}			
	} 
}

package alias PgSqlDataSet delegate(PGresult*) CompletionPointer;
package alias int delegate() SendCommandDelegate;

private struct QueuedCommand {	
	DbAction Action;
	SendCommandDelegate ExecuteMethod;
	CompletionPointer CompletionMethod;
	this(DbAction Action, SendCommandDelegate ExecuteMethod, CompletionPointer CompletionMethod) {
		this.Action = Action;
		this.ExecuteMethod = ExecuteMethod;
		this.CompletionMethod = CompletionMethod;
	}
}