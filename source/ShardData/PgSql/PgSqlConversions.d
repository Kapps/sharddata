﻿module ShardData.PgSql.PgSqlConversions;
private import ShardTools.ArrayOps;
private import std.bitmanip;
private import ShardTools.BitOps;
private import std.typecons;
private import ShardTools.ExceptionTools;
private import std.datetime;
private import std.socket;
private import std.conv;
private import core.sync.mutex;
public import ShardData.PgSql.PgSqlDefines;
public import ShardData.PgSql.PgSqlDataSet;
public import std.variant;


mixin(MakeException("UnsupportedFormatException", "Attempted to convert a value that had no converter registered for it."));

alias Variant function(DataFormat, ubyte[]) ResultConversionDelegate;
alias ubyte[] function(Variant, out DataFormat) InputConversionDelegate;

// TODO: Make this actually handle the converting, then use it for Copy operations as well.

class PgSqlConversions  {

public:

	shared static this() {
		SyncLock = new Mutex();
		AddBinary!(byte, ubyte, short, ushort, int, uint, long, ulong, char, wchar, float, double)();		
		CommandConverters[typeid(DateTime)] = (Variant Value, out DataFormat Format) {
			DateTime dt = Value.get!DateTime;
			Format = DataFormat.Text;
			return cast(ubyte[])dt.toSimpleString();
		};		
		CommandConverters[typeid(SysTime)] = (Variant Value, out DataFormat Format) {
			SysTime st = Value.get!SysTime;
			Format = DataFormat.Text;
			return cast(ubyte[])st.toSimpleString();
		};
		CommandConverters[typeid(Duration)] = (Variant Value, out DataFormat Format) {
			Duration d = Value.get!Duration;
			TickDuration td = cast(TickDuration)d;
			Format = DataFormat.Text;
			return cast(ubyte[])(to!string(td.to!("seconds", double)) ~ " seconds");
		};
		auto AddrConverter = (Variant Value, out DataFormat Format) {
			Address addr = Value.get!Address;
			Format = DataFormat.Text;
			return cast(ubyte[])addr.toAddrString();
		};
		CommandConverters[typeid(string)] = (Variant Value, out DataFormat Format) {
			string str = Value.get!string;
			Format = DataFormat.Text;
			return cast(ubyte[])str;
		};
		CommandConverters[typeid(Address)] = AddrConverter;
		CommandConverters[typeid(InternetAddress)] = AddrConverter;
		CommandConverters[typeid(Internet6Address)] = AddrConverter;
		// Add hstore support. string[string], \ is escaped.
	}

	

	private static void AddBinary(T...)() {
		synchronized(SyncLock) {
			Tuple!T Unused = tuple(T.init);
			foreach(Value; Unused) {
				alias typeof(Value) ValueType;
				InputConversionDelegate InputConverter = &(ConvertToBinary!ValueType);
				TypeInfo Type = typeid(ValueType);
				CommandConverters[Type] = InputConverter;
			}
		}
	}	

	private static ubyte[] ConvertToBinary(T)(Variant Input, out DataFormat Format) {
		Format = DataFormat.Binary;
		T Value = Input.get!T;		
		auto ValueData = nativeToBigEndian!T(Value);
		return ValueData[].dup;
	}
	
	/// Registers a DataSet converter, used to covert a column from the result of a Postgres query to a Variant.
	/// Params:
	/// 	Type = The Oid of the type to generate a converter for.
	/// 	SetConverter = The delegate to invoke to perform the conversion.
	static void RegisterConverter(Oid Type, ResultConversionDelegate SetConverter) {
		synchronized(SyncLock) {
			SetConverters[Type] = SetConverter;			
		}
	}

	/// Registers a DbCommand converter, used to convert a parameter in to a valid value for a Postgres query.
	/// Params:
	/// 	Type = The TypeInfo for the type to convert.
	/// 	CommandConverter = A delegate to invoke to perform the conversion.
	static void RegisterConverter(TypeInfo Type, InputConversionDelegate CommandConverter) {
		synchronized(SyncLock) {
			CommandConverters[Type] = CommandConverter;
		}
	}

	/// Gets the data representing the given Value as a postgres value.
	static ubyte[] ConvertToCommand(Variant Value, out DataFormat Format) {
		TypeInfo Type = Value.type;
		InputConversionDelegate Converter;
		synchronized(SyncLock) {
			Converter = CommandConverters[Type];
		}
		if(Converter)
			return Converter(Value, Format);
		throw new UnsupportedFormatException();
	}

	/// Gets a variant representing the given data.
	/// This temporarily assumes text format.
	static Variant ConvertFromSet(Oid ID, DataFormat Format, ubyte[] Data) {
		if(Format != DataFormat.Text)
			throw new NotImplementedError("Binary format is not yet implemented.");

		T con(T)(ubyte[] Data) {			
			return to!(T)(cast(string)Data);
		}
		Variant var(T)(ubyte[] Data) {
			return Variant(con!T(Data));
		}

		ResultConversionDelegate* Converter;
		synchronized(SyncLock) {
			Converter = ID in SetConverters;
		}
		if(Converter)
			return (*Converter)(DataFormat.Text, Data);

		// Special case built-ins for speed.
		// Of course, Variant at the moment has a high enough overhead that this is made pointless.
		// But eventually!
		PgOids Type = cast(PgOids)ID;
		switch(Type) {
			case PgOids.Binary:
				return Variant(cast(ubyte[])Data);
			case PgOids.Bool:
				return var!bool(Data);
			case PgOids.Char:
				return var!char(Data);
			case PgOids.DateTime:				
				return Variant(DateTime.fromSimpleString(cast(string)Data));
			case PgOids.Double:
				return var!double(Data);
			//case PgOids.Duration:
				// TODO: Implement this!
				//return Variant(Data);				
			case PgOids.Int16:
				return var!short(Data);
			case PgOids.Int32:
				return var!int(Data);
			case PgOids.Int64:
				return var!long(Data);
			case PgOids.IP:		
				// TODO: Test netmask.			
				size_t IndexMask = IndexOf(cast(string)Data, '/');
				if(IndexMask != -1)
					Data = Data[0..IndexMask];
				return Variant(parseAddress(cast(string)Data));
			case PgOids.Single:
				return var!float(Data);
			case PgOids.SysTime:
				return Variant(SysTime.fromSimpleString(cast(string)Data));
			case PgOids.Text:
			case PgOids.VarChar:
			case PgOids.Xml:
				return Variant(cast(string)Data);			
			default:
				return Variant(cast(string)Data);
		}
	}
	
private:
	static __gshared ResultConversionDelegate[Oid] SetConverters;
	static __gshared InputConversionDelegate[TypeInfo] CommandConverters;	
	static __gshared Mutex SyncLock;
}