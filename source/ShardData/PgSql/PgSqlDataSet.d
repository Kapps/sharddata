﻿module ShardData.PgSql.PgSqlDataSet;
private import ShardData.PgSql.PgSqlConversions;
private import ShardTools.ArrayOps;
private import std.conv;
version(Windows) private import std.c.windows.winsock;
private import std.range;
private import std.traits;
private import std.socket;
private import std.datetime;
private import std.array;
private import core.exception;
private import std.exception;
private import ShardData.PgSql.PgSqlConnection;
private import ShardData.PgSql.PgSqlDefines;
import std.c.stdlib;

private alias core.time.Duration Duration;

enum DataFormat {
	Text = 0,
	Binary = 1
}

/// Provides access to the results of a PgSql query.
class PgSqlDataSet : DbDataSet {

public:
	/// Initializes a new instance of the PgSqlDataSet object.
	this(PgSqlConnection Connection, size_t[string] FieldMap, PGresult* Result) {
		super(Connection);
		this.Result = Result;
		this.FieldMap = FieldMap;
		this.ParamCount = FieldMap.length;
		this.IsOpen = true;
		this.NumRows = PQntuples(Result);
		Types = (cast(Oid*)malloc(Oid.sizeof * ParamCount))[0 .. ParamCount];
		for(size_t i = 0; i < ParamCount; i++)
			Types[i] = PQftype(Result, cast(int)i);
	}

	/// Indicates the number of rows in this result set.
	@property override ulong Count() {
		return NumRows;
	}
	
	/// Gets or sets the current row that is being returned.
	/// If CanSeek is false, the set throws a NotSupportedException.
	@property override ulong Position() const {
		return CurrRow;
	}
	
	/// Ditto
	@property override void Position(ulong Value) {
		CurrRow = cast(size_t)Value;
	}	

	~this() {
		free(Types.ptr);		
	}

protected:

	/// Closes this DataSet.
	override void PerformClose() {		
		PQclear(Result);		
	}	

	/// Reads a row and advances to the next row.
	/// A NoRemainingRowsException is thrown if attempting to read past the end of a result set.
	override DataRow PerformRead() {		
		Variant[] Values = uninitializedArray!(Variant[])(ParamCount);
		for(size_t i = 0; i < ParamCount; i++) {
			Variant Val = GetValueAtCurrentRow(i);
			// TODO: Figure out a way to check if an array.
			// For array:
			//	uint -> rank
			//	uint -> flags (always 0)
			//	Oid -> element type
			//	For Dimension:
			//		uint -> Length
			//		uint -> Lower subscript bound (start index maybe?) 
			//	For Element:
			//		ubyte[] -> Value
			Values[i] = Val;
		}
		CurrRow++;
		return DataRow(FieldMap, Values);
	}

	private Variant GetValueAtCurrentRow(size_t i) {		
		// TODO: Is it possible to pre-compute this in a delegate somehow?
		// TODO: Switch to binary. Gets complicated with things like INET type though.	
		
		size_t ValLength = cast(size_t)PQgetlength(Result, cast(int)CurrRow, cast(int)i);
		char* Value = PQgetvalue(Result, cast(int)CurrRow, cast(int)i);
		//ubyte[] Data = cast(ubyte[])Value[0..ValLength];
		ubyte[] Data = cast(ubyte[])Value[0 .. ValLength];		
		Oid ID = Types[i];
		// TODO: Support non-text results.
		Variant Result = PgSqlConversions.ConvertFromSet(ID, DataFormat.Text, Data);
		return Result;
	}	
	
private:
	size_t NumRows;
	size_t ParamCount;
	size_t CurrRow;
	size_t[string] FieldMap;
	Oid[] Types;
	bool IsOpen;
	package PGresult* Result;	
}