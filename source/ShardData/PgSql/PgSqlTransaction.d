﻿module ShardData.PgSql.PgSqlTransaction;
public import ShardData.PgSql.PgSqlConnection;

public import ShardData.DbTransaction;

/// Provides access to a transaction in PSQL.
class PgSqlTransaction : DbTransaction {

public:
	
	// TODO: Make all of these methods asynchronous.

	/// Initializes a new instance of the PgSqlTransaction object.
	this(PgSqlConnection Conn) {
		super(Conn);		
	}

	/// Opens the transaction.
	override void PerformOpen() {
		Connection.QueryAsync("BEGIN;");
	}

	/// Commits this transaction.
	override void PerformCommit() {
		Connection.QueryAsync("COMMIT;");
	}
	
	/// Rolls back this transaction.
	override void PerformRollback() {
		Connection.QueryAsync("ROLLBACK;");
	}
	
private:
	
}