﻿module ShardData.PgSql.PgSqlQueryGenerator;
private import ShardTools.ArrayOps;
private import std.stdio;
private import std.array;

import ShardData.DbQueryGenerator;

/// Provides an implementation of DbQueryGenerator for postgresql.
class PgSqlQueryGenerator : DbQueryGenerator {

public:
	/// Initializes a new instance of the PgSqlQueryGenerator object.
	this() {
		
	}

protected:

	/// Override to create the header or operation info for the given query.
	/// For example, a select could return 'SELECT Fields FROM TableName'.
	/// An example of an insert would be 'INSERT INTO TableName (FieldAliases) (FieldValues).
	override string CreateOperation(in Query query, string fields) {
		string Result;
		string FixedTable = Quote(query.TableName);
		string FixedAlias = Quote(query.TableAlias);
				
		final switch(query.Type) {
			case QueryType.Call:
			case QueryType.Insert:
			case QueryType.Unknown:
			case QueryType.Update:
				throw new NotImplementedError("CreateOperation for Call, Insert, Unknown, Update");				
				//Result ~= "INSERT INTO " ~ query.TableName ~ " (" ~ fields ~ ") VALUES ()";
				//if(query.TableAlias != query.TableName)
					//throw new NotSupportedException("Aliases are not supported for inserts.");				
				//break;
			case QueryType.Select:				
				if(fields is null)
					Result ~= "SELECT\r\n\t*\r\nFROM\r\n\t" ~ FixedTable;
				else
					Result ~= "SELECT\r\n\t" ~ fields ~ "\r\nFROM\r\n\t" ~ FixedTable;
				if(query.TableAlias != query.TableName && query.TableAlias !is null)
					Result ~= " AS " ~ FixedAlias;				
		}
		return Result;
	}

	/// Override to handle parsing the offset and count for this query.
	override string ParseLimit(in Query query) {
		string LimitResult;
		if(query.Start !is null)
			LimitResult ~= "\r\nOFFSET\r\n\t" ~ ParseExpression(*(query.Start));
		if(query.Count !is null)
			LimitResult ~= "\r\nLIMIT\r\n\t" ~ ParseExpression(*(query.Count));
		return LimitResult;
	}

	private static string Quote(string Input) {
		string Result = "";
		auto SplitRes = split(Input, ".");					
		foreach(Index, Part; SplitRes) {
			if(Part.length > 0 && Part[0] != '\"')
				Result ~= '\"' ~ Part ~ '\"';
			else
				Result ~= Part;
			if(Index != SplitRes.length - 1)
				Result ~= '.';
		}
		return Result;
	}
	
private:
}