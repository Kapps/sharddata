﻿module ShardData.DataRow;
private import std.bitmanip;
import std.traits;
import std.variant;
import std.exception;
import std.conv : to;
import std.c.stdlib;
import core.memory;

/// Represents a single row of data.
struct DataRow  {

	// TODO: Change Variant to something significantly faster. Like ubyte[].

	//mixin(Mixins.CreateLowercaseAliases!(typeof(this)));

	/// Creates a new DataRow with the given Fields and Values.
	/// Params:
	/// 	Fields = A map indicating the relationship between field names and indices.
	/// 	Values = The raw data for each value in this row.
	this(size_t[string] Fields, Variant[] Values) {
		this.FieldToIndex = Fields;
		this.Data = Values;						
		/+if(SwapEndian) {
			this.RequiresSwap = cast(bool*)GC.malloc(Data.length);
			for(size_t i = 0; i < Data.length; i++)
				this.RequiresSwap[i] = true;
		}+/
	}

	/// Gets the value of the given field.
	/// For arrays, the value should be copied if intended to be used longer than the duration of this row's lifespan.
	/// Params:
	/// 	FieldName = The name of the field to get the value for.
	/// 	Index = The index to get the field for. Can be found with IndexForField.
	T Get(T)(string FieldName) {
		return Get!(T)(IndexForField(FieldName));
	}

	/+ /// Ditto
	T Get(T)(int Index) {		
		static if(!isArray!(T)) {
			ubyte[] Result = Data[Index];						
			enforce(T.sizeof == Result.length, "Expected result to be " ~ to!string(T.sizeof) ~ " bytes, but got " ~ to!string(Result.length) ~ " bytes instead.");
			T Converted = *(cast(T*)Result.ptr);
			static if(T.sizeof > 1) { 
				if(this.RequiresSwap && this.RequiresSwap[Index]) {
					this.RequiresSwap[Index] = false;					
					return bigEndianToNative!T(*(cast(ubyte[T.sizeof]*)&Converted));
				}
			}			
			return Converted;			
		} else {
			alias std.range.ElementEncodingType!T ElementType;
			enum ElementSize = ElementType.sizeof;			
			ubyte[] Result = Data[Index];	
			enforce(Result.length % ElementSize == 0, "Invalid array for Get; Data length was not a multiple of element size.");
			ElementType[] Elements = cast(ElementType[])Result;
			static if(ElementSize > 1) {
				if(this.RequiresSwap && this.RequiresSwap[Index]) {
					this.RequiresSwap[Index] = false;
					foreach(ref Element; Elements)						
						Element = bigEndianToNative!ElementType(*(cast(ubyte[ElementSize]*)&Element));					
				}
			}
			return cast(T)(Result);
		}
	}+/

	T Get(T)(size_t Index) {
		return Data[Index].get!T;
	}

	/// Gets the integer index for the given field name, or -1 if not found.
	/// Note that it is more efficient to store this than to access by name multiple times.
	/// Params:
	/// 	FieldName = The name of the field to get the index for.
	size_t IndexForField(string FieldName) {
		size_t* Result = (FieldName in FieldToIndex);
		if(Result)
			return *Result;
		return -1;
	}

	/// Gets the names of all of the fields in this row.
	string[] FieldNames() const {
		return FieldToIndex.keys;
	}

	T opIndex(T)(string FieldName) {
		return Get!T(FieldName);
	}

private:
	size_t[string] FieldToIndex;	
	Variant[] Data;	
}