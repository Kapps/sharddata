﻿module ShardData.Query;
private import std.traits;
private import std.conv;
private import std.exception;
private import std.string;
public import ShardData.DbConnection;
public import ShardData.DbCommand;

/// Indicates the operation a query performs.
enum QueryType {
	/// The operation is unknown, or not a predefined operation.
	Unknown,
	/// A select operation is performed.
	Select = 1,
	/// A row insertion is performed.
	Insert,	
	/// A function or prepared statement is aclled.
	Call,
	/// An update operation is performed.
	Update
}

/// Indicates the type of a query condition.
enum ExpressionType {
	/// A raw string is passed in for the condition.
	Raw = 0,
	/// A sub query is parsed for the condition.
	SubQuery = 1
}

/// Indicates the way to order the results of a query.
enum QueryOrder {	
	Undefined = 0,
	Ascending = 1,
	Descending = 2
}

/// Provides a condition or expression in a query.
struct QueryExpression {
	union {
		/// The raw expression for this condition. This is just a string passed in.
		const string Raw;
		/// A query to be parsed to generate the expression for the condition.
		const(Query) SubQuery;
	}
	/// The type of the condition.
	ExpressionType Type;

	this(string Raw) {
		this.Raw = Raw;
		this.Type = ExpressionType.Raw;
	}

	this(in Query SubQuery) {
		this.SubQuery = SubQuery;
		this.Type = ExpressionType.SubQuery;
	}
}

/// Represents a field to be selected from a query.
struct QueryField {
	/// The name or expression for the field. This can be the same as Alias, and should never be null.
	const QueryExpression Expression;
	/// The alias for the field. This value can be null.
	const string Alias;

	this(in QueryExpression Expression, string Alias) {		
		this.Expression = Expression;
		this.Alias = Alias;
	}

	this(string Name) {
		this(QueryExpression(Name), null);		
	}

	this(string Expression, string Name) {
		this(QueryExpression(Expression), Name);		
	}

	this(in Query SubQuery, string Name) {
		this(QueryExpression(SubQuery), Name);		
	}
}

/// Used to generate a query for selecting fields from a table.
///	Note that conditions are sent raw and are $(B, NOT) escaped).
/// This means when passing an unknown value to a condition, you should always use a parameter.
/// At the moment, there is no naming scheme for database compatible parameters.
/// As such, you will need to change the parameter name format when switching databases (such as from @Name to :Name).
/// It is allowed for a database to extend this class to add custom database-specific methods, but at that point it stops being cross-database.
/// Passing in an extended Query to a query generator that does not expect it will likely result in clauses it does not recognize being ignored.
class Query {	

	// TODO: Consider supporting per-column ordering.

	/// The name of the table being selected from.
	const string TableName;

	/// The alias to give to the table.
	/// If no alias is specified, this is null.
	const string TableAlias;

	/// Indicates the type of this Query.
	const QueryType Type;

	/// Creates a query on the given table.
	/// Params:
	/// 	TableName = The name of the table being selected from.
	/// 	Type = Indicates the type of this Query.
	///		TableAlias = The name to refer to the table by. Can be null to be the same value as TableName.
	this(string TableName, string TableAlias, QueryType Type) {
		this.TableName = TableName;
		if(TableAlias.length > 0)
			this.TableAlias = TableAlias;
		else
			this.TableAlias = this.TableName;		
		this.Type = Type;		
	}

	/// Gets the fields being selected.
	@property const(QueryField[]) Fields() const {
		return _Fields;		
	}

	/// Provides a list of filters (where clauses) for the query.
	@property const(QueryExpression[]) Filters() const {
		return _Where;
	}

	/// Indicates how this query is ordered.
	@property const(QueryExpression[]) Orders() const {
		return _Order;
	}

	/// Gets the values being used for this query.
	/// This is not the actual values for a DbCommand, but usually the names of the parameters.
	/// In particular, this is used for the Values portion of an update/insert statement, or the parameters for a stored procedure or function.
	@property const(QueryExpression[]) Parameters() const {
		return _Parameters;
	}

	/// Adds the given fields to be selected.
	/// Allowed types are string, for a raw column name with no alias, QueryField to be passed in directly, 
	/// a tuple containing a raw column name and alias (in that order), or a tuple containing a Query and alias.
	Query Select(T...)(T Fields) {		
		foreach(Field; Fields) {
			alias typeof(Field) FieldType;
			static if(is(FieldType == string))
				_Fields ~= QueryField(Field);
			else static if(is(FieldType == QueryField))
				_Fields ~= Field;
			else static if(is(FieldType == Tuple!(string, string)))
				_Fields ~= QueryField(Field[0], Field[1]);
			else static if(is(FieldType == Tuple!(Query, string)))
				_Fields ~= QueryField(Field[0], Field[1]);
			else 
				static assert(0, "Fields must contain only strings (column name) or QueryField instances.");			
		}
		return this;
	}

	/// Adds the given conditions to the filters for this query.
	/// Allowed types are a string for a raw value, a QueryExpression instance to be passed in directly, or a Query for a subquery.
	Query Filter(T...)(T Conditions) {
		enforce(this.Type == QueryType.Select || this.Type == QueryType.Update);		
		foreach(Condition; Conditions) {
			alias typeof(Condition) ConType;
			static if(is(ConType == string))
				this._Where ~= QueryExpression(Condition);
			else static if(is(ConType == QueryExpression))
				this._Where ~= Condition;
			else static if(is(ConType == Query))
				this._Where ~= QueryExpression(Condition);
			else
				static assert(0, "Unknown parameter type for filter.");
		}
		return this;
	}

	/// Adds the given expressions to the Parameters clause.
	/// Allowed types are a string for a raw value, a QueryExpression instance to be passed in directly, or a Query for a subquery.
	Query Param(T...)(T Params) {
		enforce(this.Type == QueryType.Call || this.Type == QueryType.Update || this.Type == QueryType.Insert);
		foreach(Parameter; Params) {
			alias typeof(Parameter) ParamType;
			static if(is(ParamType == string))
				this._Values ~= QueryExpression(Parameter);
			else static if(is(ParamType == QueryExpression))
				this._Values ~= Parameter;
			else static if(is(ParamType == Query))
				this._Values ~= QueryExpression(Parameter);
			else
				static assert(0, "Unknown parameter type for value.");
		}
		return this;
	}

	/// Adds the given expressions to use to order this query.
	/// Allowed types are string for a raw value, a raw QueryExpression, a Query instance for a subquery, or a QueryOrder value (limited to one per call and replaces existing value).
	Query OrderBy(T...)(T Orders) {
		enforce(this.Type == QueryType.Select);
		bool HasOrder = false;
		foreach(Parameter; Orders) {
			alias typeof(Parameter) ParamType;
			static if(is(ParamType == string))
				this._Order ~= QueryExpression(Parameter);
			else static if(is(ParamType == QueryExpression))
				this._Order ~= Parameter;
			else static if(is(ParamType == Query))
				this._Order ~= QueryExpression(Parameter);			
			else static if(is(ParamType == QueryOrder)) {
				// TODO: Do this at compile-time somehow.
				enforce(!HasOrder, "An order was already defined in this call. Only a single order is allowed. This means you may not specify a different order for different columns.");
				HasOrder = true;
				this._OrderType = Parameter;
			} else
				static assert(0, "Unknown parameter type in order by clause.");
		}
		return this;
	}

	/// Gets the number of rows to skip, or null if not set.
	@property const(QueryExpression)* Start() const {		
		return _Start;
	}

	/// Gets the maximum number of rows to return, or null if not set.
	@property const(QueryExpression)* Count() const {
		return _Count;
	}
	
	/// Indicates in what way the query is ordered.
	@property QueryOrder OrderType() const {
		return _OrderType;
	}

	/// Assigns the given value as the offset for this query.
	/// Valid parameter types are string, an integer value (converted to string), QueryExpression, or Query, each of which get converted to a QueryExpression.
	/// If typeof(null) is passed in, the offset is cleared.
	Query Skip(T)(in T Value) {
		static assert(is(T == string) || is(T == Query) || is(T == QueryExpression) || is(T == typeof(null)) || isIntegral!T, "Invalid parameter type for offset.");
		static if(is(T == QueryExpression)) {
			if(Value.Type == ExpressionType.Raw)
				Skip(Value.Raw);
			else if(Value.Type == ExpressionType.SubQuery)
				Skip(Value.SubQuery);
			else assert(0);
		} else static if(is(T == typeof(null)))
			this._Start = null;
		else static if(isIntegral!T)
			Skip(to!string(Value));
		else
			this._Start = new QueryExpression(Value);
		return this;
	}

	/// Limits this query to the given number of rows returned.
	/// Valid parameter types are string, an integer value (converted to string), QueryExpression, or Query, each of which get converted to a QueryExpression.
	/// If typeof(null) is passed in, the limit is cleared.
	Query Limit(T)(in T Value) {
		static assert(is(T == string) || is(T == Query) || is(T == QueryExpression) || is(T == typeof(null)) || isIntegral!T, "Invalid parameter type for limit.");
		static if(is(T == QueryExpression)) {
			if(Value.Type == ExpressionType.Raw)
				Limit(Value.Raw);
			else if(Value.Type == ExpressionType.SubQuery)
				Limit(Value.SubQuery);
			else assert(0);
		} else static if(is(T == typeof(null)))
			this._Count = null;
		else static if(isIntegral!T)
			Limit(to!string(Value));
		else
			this._Count = new QueryExpression(Value);
		return this;
	}	
	
private:
	QueryField[] _Fields;
	QueryExpression[] _Where;
	QueryExpression[] _Order;
	QueryExpression[] _Parameters;
	QueryOrder _OrderType;
	QueryExpression* _Start;
	QueryExpression* _Count;	
}