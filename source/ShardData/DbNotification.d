﻿module ShardData.DbNotification;
private import ShardTools.ExceptionTools;
private import ShardTools.Event;
private import ShardData.DbConnection;

struct NotificationDetails {
	string Payload;
	string[string] AdditionalInfo;
	DbNotification Notifier;
}

/// Provides accss to a single notify/listen queue for a database.
class DbNotification  {

public:
	
	alias Event!(void, NotificationDetails) QueueEvent;

	/// Initializes a new instance of the DbNotification object.
	this(string Queue, DbConnection Connection) {
		this._Queue = Queue;
		this._Connection = Connection;
		this._NotificationReceived = new QueueEvent();
	}

	/// Gets an event called when a notification is received on this instance.
	@property QueueEvent NotificationReceived() {
		return _NotificationReceived;
	}

	/// Gets the queue that this notification system listens on.
	@property string Queue() const {
		return _Queue;
	}

	/// Indicates whether this instance is currently listening for notifications.
	@property bool IsListening() const {
		return _IsListening;
	}

	/// Gets the connection that this notification applies for.
	@property DbConnection Connection() {
		return _Connection;
	}

	/// Begins listening for notifications.
	void Listen() {
		synchronized(this) {
			if(_IsListening)
				throw new InvalidOperationException("The notification is already being listened for.");		
			this._IsListening = true;
			PerformListen();			
		}
	}

	/// Stops listening for notifications.
	void Unlisten() {
		synchronized(this) {
			if(!_IsListening)
				throw new InvalidOperationException("The notification is not currently listening.");			
			PerformUnlisten();
			this._IsListening = false;
		}
	}

	/// Sends a notification to this queue with the given payload.
	/// Params:
	/// 	Payload = Any additional information to send with this notification, provided that the database supports it.
	void SendNotification(string Payload) {
		PerformSendNotification(Payload);
	}

	~this() {
		// TODO: Decide what to do here.
		if(IsListening)
			Unlisten();
	}

protected:

	/// Override to begin listening for notifications.
	/// When a notification is received, the NotifyNotificationReceived method should be called.
	abstract void PerformListen();

	/// Override to stop listening for notifications.
	abstract void PerformUnlisten();

	/// Override to send a notification to this queue, with the given additional information or payload.
	abstract void PerformSendNotification(string Payload);

	/// Called to indicate that a notification was received.
	/// Params:
	/// 	Details = Any details about the notification.
	void NotifyNotificationReceived(string Payload, string[string] Details) {
		synchronized(this) {			
			if(!IsListening)
				throw new InvalidOperationException("Unable to notify of a receive when not listening.");
		}
		NotificationDetails ND;
		ND.Payload = Payload;
		ND.AdditionalInfo = Details;
		ND.Notifier = this;
		NotificationReceived.Execute(ND);
	}
	
private:
	DbConnection _Connection;
	string _Queue;
	bool _IsListening;
	QueueEvent _NotificationReceived;
}