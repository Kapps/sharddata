﻿module ShardData.NotificationCachePolicy;
private import std.stdio;
private import ShardData.DbNotification;

import ShardData.DbCachePolicy;

/// Provides a cache policy that uses a DbNotification to indicate when the query needs to be re-executed.
class NotificationCachePolicy : DbCachePolicy {

public:
	/// Initializes a new instance of the NotificationCachePolicy object.
	/// Params:
	/// 	Notification = The notification to use to signify that the cache is invalid. If it is not currently listening, it will have Listen called.
	this(DbNotification Notification) {
		this._Notification = Notification;
		if(!_Notification.IsListening)
			_Notification.Listen();
		_Notification.NotificationReceived.Add(&OnNotificationReceived);
	}

	/// Gets the notification that is used to invalidate this cache.	
	@property DbNotification Notification() {
		return _Notification;
	}

protected:

	/// Called when a notification is received.
	void OnNotificationReceived(NotificationDetails Details) {		
		this.Invalidate();
	}	
	
private:
	DbNotification _Notification;
}