﻿module ShardData.DataContext; 
private import ShardTools.StringTools;
private import ShardData.Table;
private import ShardData.Query;


/// Provides access to a given database through helper methods to generate queries.
class DataContext  {

public:
	/// Initializes a new instance of the DataContext object.
	this(DbConnection Connection) {
		this._Connection = Connection;
	}

	/// Gets the connection used for this DataContext.
	@property DbConnection Connection() {
		return _Connection;
	}

	/// Gets or lazily creates a table capable of storing objects of the given type.
	@property Table!T Tables(T)() {
		synchronized(this) {
			TypeInfo TI = typeid(T);
			auto In = (TI in CreatedTables);
			if(In)
				return cast(Table!T)(*In);
			auto Result = new Table!T(T.name, Connection);
			CreatedTables[TI] = cast(void*)Result;
			return Result;
		}
	}
	
private:
	void*[TypeInfo] CreatedTables;
	DbConnection _Connection;
}