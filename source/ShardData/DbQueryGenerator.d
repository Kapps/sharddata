﻿module ShardData.DbQueryGenerator;
private import std.exception;
private import ShardTools.ExceptionTools;
public import ShardData.Query;

mixin(MakeException("InvalidQueryException"));

/// Provides a class that generates Sql queries for certain operations.
/// This class is used to implement DataContexts/Tables as well as Query parsing.
/// It is generally not accessed directly.
class DbQueryGenerator  {

public:
	/// Initializes a new instance of the DbQueryGenerator object.
	this() {
		
	}
	
	/// Returns a string representing the Sql the given query would generate for this database.
	string ParseQuery(in Query query) {
		string FieldData = ParseFields(query);
		string Op = CreateOperation(query, FieldData);
		string WhereData = ParseWhere(query);
		string OrderData = ParseOrder(query);
		string LimitData = ParseLimit(query);
		return Op ~ WhereData ~ OrderData ~ LimitData;
	}

protected:

	/// Gets the string representing the given expression.
	string ParseExpression(in QueryExpression Expression) {
		if(Expression.Type == ExpressionType.Raw)
			return Expression.Raw;
		else if(Expression.Type == ExpressionType.SubQuery)
			return ParseQuery(Expression.SubQuery);
		else assert(0);
	}

	/// Override to create the header or operation info for the given query.
	/// For example, a select could return 'SELECT Fields FROM TableName'.
	/// An example of an insert would be 'INSERT INTO TableName (FieldAliases) (FieldValues).
	abstract string CreateOperation(in Query query, string fields);

	/// Parses the fields from the given query.
	string ParseFields(in Query query) {
		string FieldData;		
		foreach(Index, Field; query.Fields) {
			if(Index != 0)
				FieldData ~= ", ";
			if(Field.Alias == null) {				
				enforceEx!InvalidQueryException(Field.Expression.Type == ExpressionType.Raw, "If using an expression for a field, an alias must be specified.");
				FieldData ~= ParseExpression(Field.Expression);
			} else {				
				if(Field.Expression.Type == ExpressionType.Raw)
					FieldData ~= ParseExpression(Field.Expression) ~ " AS " ~ FormatAlias(query, Field.Alias);
				else
					FieldData ~= '(' ~ ParseExpression(Field.Expression) ~ ") AS " ~ FormatAlias(query, Field.Alias);
			}
		}
		return FieldData;
	}

	/// Gets the where clause from the given query (including the WHERE keyword).
	string ParseWhere(in Query query) {
		string WhereResult;
		if(query.Filters.length > 0)
			WhereResult ~= "\r\nWHERE\r\n\t";
		foreach(Index, Condition; query.Filters) {			
			if(Index != 0) {
				if(Index % 2 == 0)
					WhereResult ~= "\r\n\tAND ";
				else
					WhereResult ~= " AND ";
			}
			string Parsed = ParseExpression(Condition);
			WhereResult ~= '(' ~ Parsed ~ ')';
		}
		return WhereResult;
	}

	/// Parses the order by clause from the given query (including the ORDER BY keywords).
	string ParseOrder(in Query query) {
		string OrderResult;
		if(query.Orders.length > 0)
			OrderResult ~= "\r\nORDER BY\r\n\t";
		foreach(Index, Condition; query.Orders) {
			if(Index != 0) {
				if(Index % 2 == 0)
					OrderResult ~= "\r\n\t, ";
				else
					OrderResult ~= ", ";
			}
			string Parsed = ParseExpression(Condition);
			OrderResult ~= '(' ~ Parsed ~ ')';
		}
		final switch(query.OrderType) {
			case QueryOrder.Ascending:
				OrderResult ~= " ASC";
				break;
			case QueryOrder.Descending:
				OrderResult ~= " DESC";
				break;
			case QueryOrder.Undefined:
				break;
		}
		return OrderResult;
	}
	
	/// Returns the formatted string for the given alias.
	string FormatAlias(in Query query, string Alias) {
		return '\"' ~ Alias ~ '\"';
	}	

	/// Override to handle parsing the offset and count for this query.
	abstract string ParseLimit(in Query query);

private:
}