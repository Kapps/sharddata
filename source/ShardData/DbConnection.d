﻿module ShardData.DbConnection;
public import ShardData.IDbConnectionPool;
private import core.atomic;
public import ShardData.DbQueryGenerator;
private import std.concurrency;
private import std.parallelism;
private import ShardTools.LinkedList;
private import core.sync.mutex;
private import core.thread;
public import ShardData.DbAction;
public import ShardData.DataContext;
public import ShardData.LocalTransaction;

public import ShardData.DbConnectionInfo;
public import ShardData.DbCommand;
public import ShardData.DbTransaction;

public import ShardData.Query;

private import ShardTools.ExceptionTools;

/// Determines the state of a Database Connection.
enum DbConnectionState {
	Closed = 0,
	Open = 1
}

mixin(MakeException("ConnectionStateException", "The database connection was in an invalid state for this operation."));
mixin(MakeException("ConnectionFailedException", "The connection attempt to the host did not succeed."));

/// The base class for a connection to an SQL database.
/// Note that this class and all similar classes (such as commands or data sets) are $(B, NOT) thread safe.
/// Bugs:
///		Timeout is not yet implemented.
abstract class DbConnection  {

public:
	//mixin(Mixins.CreateLowercaseAliases!(typeof(this)));

	// TODO: Create a Condition for when the connection is closed while a command is attempting to be executed.
	// The ConnectionPool in particular, and probably the default implementation of the DbConnection, should attempt to re-open the connection and re-execute.

	/// Initializes a new instance of the DbConnection object.
	/// An oveloaded constructor with these parameters (taking in a derived ConnectionInfo) must exist.
	/// Params:
	///		ConnectionInfo = Determines how to open the connection.
	this(DbConnectionInfo ConnectionInfo) {
		// TODO: Automatic connection pooling by ConnectionInfo.
		// Perhaps with a StoreState and LoadState method in DbConnection, then connection pooling is done per class with helpers.
		this.ConnectionInfo = ConnectionInfo;
		_State = DbConnectionState.Closed;
		_Timeout = Duration.init;
	}

	/// Returns a query generator instance used to generate queries for this particular database.	
	@property abstract DbQueryGenerator QueryGenerator();

	/// Gets the currently active transaction on this connection.
	/// Only one transaction may be active at a time (that is, nested transactions are not allowed).
	@property final DbTransaction ActiveTransaction() {
		return _ActiveTransaction;
	}

	/// Gets or sets the duration that must pass before a query times out.	
	/// A value of Duration.init indicates no timeout.
	/// This value is not yet implemented for the most part.
	@property Duration Timeout() const {		
		return _Timeout;
	}

	/// Ditto
	@property void Timeout(Duration Value) {		
		synchronized(this) {
			_Timeout = Value;		
		}
	}

	/// Gets a lazily created a DataContext to use with this connection.
	/// This is a convenience method, and a single connection may have multiple contexts by manually creating them.
	@property DataContext Context() {		
		synchronized(this) {
			if(_Context is null)
				_Context = new DataContext(this);
			return _Context;		
		}
	}
	
	/// Gets the state of this connection.
	@property DbConnectionState State() {
		return _State;
	}

	/// Gets or sets the connection string for this connection.
	/// If the connection is already open, attempting to set the connection string will generate an exception.
	/// Params:
	/// 	ConnectionInfo = The connection info to set for the connection.
	@property void ConnectionInfo(DbConnectionInfo ConnectionInfo) {		
		synchronized(this) {
			if(State == DbConnectionState.Open)
				throw new ConnectionStateException("The connection was already open, unable to set connection info.");
			this._ConnectionInfo = ConnectionInfo;
		}
	}

	/// Ditto
	@property DbConnectionInfo ConnectionInfo() {
		return this._ConnectionInfo;
	}

	/// Opens this connection, throwing an exception if it is already open.
	void Open() {
		synchronized(this) {
			if(State == DbConnectionState.Open)
				throw new ConnectionStateException();		
			PerformOpen();
			this._State = DbConnectionState.Open;
		}
	}

	/// Closes this connection, throwing an exception if it is already closed.
	void Close() {
		synchronized(this) {
			EnforceOpen();
			PerformClose();
			this._State = DbConnectionState.Closed;
		}
	}

	/// Performs a parameterized raw query on the database, generally without using a prepared statement.	
	/// While query is not escaped in any way, parameters will be free from sql injection.
	/// Returns:
	///		The number of rows affected by this query.
	final DbAction QueryAsync(string Query, string[] Params ...) {		
		synchronized(this) {
			EnforceOpen();
			auto Result = PerformQueryAsync(Query, Params);
			Result.TimeoutTime = this.Timeout;
			return Result;
		}
	}

	/// Ditto
	final DbDataSet Query(string Query, string[] Params ...) {
		DbAction Result = QueryAsync(Query, Params);
		CompletionType Type = Result.WaitForCompletion();
		if(Type != CompletionType.Successful)
			throw cast(Throwable)Result.CompletionData;
		return cast(DbDataSet)Result.CompletionData;
	}

	/// Creates a transaction for this connection.
	/// Creating transactions is an asynchronous operation, as commands are queued.
	final LocalTransaction StartTransaction() {
		synchronized(this) {
			EnforceOpen();
			return LocalTransaction(CreateTransaction());
		}
	}	

	/// Creates and returns a command with a default name with the connection set to this instance.
	final DbCommand CreateCommand() {
		synchronized(this) {
			EnforceOpen();
			return PerformCreateCommand();
		}
	}	

	~this() {
		if(State == DbConnectionState.Open)
			Close();		
	}

	/+ /// Parses the given query, returning the resulting query string.
	abstract string ParseQuery(Query Query);+/

protected:
	/// Override to perform the actual opening of the connection.
	abstract void PerformOpen();
	/// Override to execute a raw query.
	abstract DbAction PerformQueryAsync(string Query, string[] Params);
	/// Override to close the connection.
	abstract void PerformClose();
	/// Override to create a new command on this connection.
	abstract DbCommand PerformCreateCommand();
	/// Override to create a new Transaction on this connection.
	/// This should be asynchronous, but queued.
	abstract DbTransaction CreateTransaction(); 	
	
	DbConnectionState _State;
	package DbTransaction _ActiveTransaction;	

private:	
	bool _UseSSL;
	bool _UseCompression;
	DbConnectionInfo _ConnectionInfo;
	DataContext _Context;		
	Duration _Timeout;	
	IDbConnectionPool _Pool;
	
	void EnforceOpen() {
		if(State != DbConnectionState.Open)
			throw new ConnectionStateException();
	}
}	
