﻿module ShardData.IDbConnectionPool;


/// Provides a non-templated interface for a connection pool.
interface IDbConnectionPool  {
	/// Gets or sets the minimum or maximum number of connections to be executing a command at once for this pool.
	/// Lowering the maximum connections will not close any existing connections until they are done their current query.
	/// For performance reasons, it is possible for the actual number of connections to be slightly higher than the maximum due to race conditios.
	@property size_t MaxConnections() const;
	/// Ditto
	@property void MaxConnections(size_t Value);
	/// Ditto
	@property size_t MinConnections() const;
	/// Ditto
	@property void MinConnections(size_t Value);
}