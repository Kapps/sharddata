﻿module ShardData.DbDataSet;
private import core.atomic;
private import ShardTools.ExceptionTools;
public import std.typecons;
import ShardData.DataRow;
import ShardData.DbConnection;

mixin(MakeException("NoRemainingRowsException", "Attempted to read past the last row in a result set."));


/// Handles the data returned by a database query.
/// This class is not thread safe.
abstract class DbDataSet  {

public:
	
	// TODO: RowsAffected property.

	/// Initializes a new instance of the DbDataSet object.
	this(DbConnection Connection) {
		this._Connection = Connection;
		this._IsOpen = true;
	}
	
	/// Gets the connection that this DataSet belongs to.
	@property DbConnection Connection() {
		return _Connection;
	}	

	/// Indicates whether this result set is open.
	@property bool IsOpen() const {
		return _IsOpen;
	}

	/// Closes this DataSet. If the data set belongs to a cache, it will not be closed unless Forced is set to true.
	/// Params:
	/// 	Forced = Indicates whether to close the data set even if it is currently being cached (and thus invalidate the cache).
	/// Returns:
	/// 	Whether the data set was closed, as opposed to being used for a cache still.
	final bool Close(bool Forced = false) {
		if(_Cache !is null && !Forced) {
			// Still being used for a cache. Make sure to reset position though.			
			this.Position = 0;
			return false;
		}
		if(!cas(cast(shared)&_IsOpen, cast(shared)true, cast(shared)false))
			throw new InvalidOperationException("The data set was already closed.");		
		if(_Cache !is null && Forced)
			_Cache.Invalidate();
		PerformClose();	
		return true;			
	}	
	
	/// Indicates the number of rows in this result set.
	@property abstract ulong Count();

	/// Gets or sets the current row that is being returned.	
	@property abstract ulong Position();

	/// Ditto
	@property abstract void Position(ulong Value);

	/// Closes this result set when the opApply operator is complete. Returns the same instance of this data set.	
	ref DbDataSet CloseOnIteration() {
		_CloseOnIteration = true;
		return this;
	}

	/// Reads a row and advances to the next row.
	/// Exceptions:
	///		NoRemainingRowsException = Thrown if attempting to read past the end of a result set.
	/// 	InvalidOperationException = Thrown if attempting to read after the data set has been closed.
	final DataRow Read() {
		if(!_IsOpen)
			throw new InvalidOperationException("Unable to read from a closed data set.");
		if(Position >= Count)
			throw new NoRemainingRowsException();
		return PerformRead();
	}

	// TODO: Make this an input range.
	int opApply(scope int delegate(DataRow) dg) {
		int Result = 0;
		while(Position < Count) {
			DataRow Current = Read();			
			if((Result = dg(Current)) != 0)
				break;
		}
		if(_CloseOnIteration)
			Close();
		return Result;
	}

	~this() {		
		if(_IsOpen)
			Close(true);
	}

protected:

	/// Called when the result set should be closed.
	abstract void PerformClose();

	/// Called to read a single row from the data set.
	abstract DataRow PerformRead();

private:
	package DbCachePolicy _Cache;
	DbConnection _Connection;	
	bool _IsOpen;
	bool _CloseOnIteration;
}