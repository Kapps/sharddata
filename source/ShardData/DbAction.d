﻿module ShardData.DbAction;
public import ShardData.DbConnection;
private import std.exception;
public import ShardData.DbDataSet;

public import ShardTools.AsyncAction;
import ShardTools.Untyped;

/// Provides an asynchronous action for executing a database call asynchronously.
/// The completion data is a DbDataSet representing the result of the command, or an instance of Throwable.
class DbAction : AsyncAction {

public:
	/// Initializes a new instance of the DbAction object.
	this(DbConnection Connection) {
		this._Connection = Connection;
		this.TimeoutTime = Connection.Timeout;
	}

	/// Gets the connection that this action is using for operations.
	@property DbConnection Connection() {
		return _Connection;
	}

	/// Indicates whether this action can be canceled, disregarding the current completion state of the action.
	/// At the moment, aborting a DbAction is not supported.
	@property override bool CanAbort() const {
		return false;
	}

	/// Completes this action with the given DataSet as the result.
	/// An error is thrown if the action is already complete.
	void NotifyResult(DbDataSet Set) {		
		enforce(!IsComplete);		
		NotifyComplete(CompletionType.Successful, Untyped(Set));		
	}

	/// Notifies this action that executing the command failed with the given error.
	void NotifyFailure(Throwable ex) {
		NotifyComplete(CompletionType.Aborted, Untyped(ex));
	}

protected:

	/// Implement to handle the actual cancellation of the action.
	/// If an action does not support cancellation, CanAbort should return false, and this method should throw an error.
	override void PerformAbort() {
		assert(0);
	}
	
private:
	DbConnection _Connection;
}