﻿/// Represents a single transaction within an SQL database, usually using a call to BEGIN.
/// This class provides only the base methods available for all databases.
/// In this case, only Commit or Rollback are provided, with small wrappers such as to check if complete.
/// Usually, you will want to use a LocalTransaction instead of using this class directly.
/// If a transaction gets garbage collected prior to being completed, a TransactionAbandonedError is thrown.
/// Transaction nesting is generally not supported by most databases, and thus not supported by this class.
/// If transactions are attemped to be nested, a TransactionExistsException is thrown.
/// ---
/// auto Trans = Connection.BeginTransaction();
/// scope(failure)
///		Trans.Rollback();
/// Command.ExecuteScalar(FirstParams);
/// Command.ExecuteScalar(SecondParams);
/// Trans.Commit();
/// ---
module ShardData.DbTransaction;
private import ShardTools.ExceptionTools;
import ShardData.DbConnection;

mixin(MakeException("TransactionAbandonedError", "A transaction was destroyed prior to a commit or rollback. The transaction has been rolled back, but the program is not in a usable state and data loss is likely."));
mixin(MakeException("TransactionCompleteException", "Unable to complete a transaction because it was already complete."));
mixin(MakeException("TransactionExistsException", "A transaction was already active on the connection and nested transactions are not allowed."));

/// Represents a single transaction in the database.
/// A transaction is not committed until either Commit or Rollback is called.
/// If the transaction gets garbage collected prior to either of these, Rollback is called.
/// Opening, closing, and rolling back a transaction are asynchronous methods.
/// However, because only one command may actually be executed at a time, it is transparently applied to the connection.
/// If multiple commands are supported, then these methods must be changed to be synchronous.
abstract class DbTransaction  {

public:
	/// Initializes a new instance of the DbTransaction object.
	/// Params:
	/// 	Connection = The database connection to use for this transaction.
	this(DbConnection Connection) {
		this._Connection = Connection;
		if(_Connection._ActiveTransaction !is null)
			throw new TransactionExistsException();
		_Connection._ActiveTransaction = this;
		PerformOpen();		
		_NeedsRollback = true;
	}

	/// Gets the connection being used for this transaction.
	@property DbConnection Connection() {
		return _Connection;
	}
	
	/// Commits this transaction.
	final void Commit() {		
		synchronized(this) {
			PerformCommit();
			Complete();
		}
	}
	
	/// Rolls back this transaction.
	final void Rollback() {		
		synchronized(this) {
			PerformRollback();
			Complete();
		}
	}

	/// Returns whether the transaction was completed through either a commit or a rollback.
	final @property bool IsComplete() const {
		return !_NeedsRollback;
	}

	~this() {
		if(!IsComplete) {
			PerformRollback();
			throw new TransactionAbandonedError();
		}
	}
	
protected:
	
	/// Commits this transaction.
	abstract void PerformCommit();

	/// Performs a rollback operation.
	abstract void PerformRollback();

	/// Opens the transaction.
	abstract void PerformOpen();

private:
	DbConnection _Connection;
	bool _NeedsRollback;
	bool _Started;

	void Complete() {
		if(!_NeedsRollback)
			throw new TransactionCompleteException();
		_NeedsRollback = false;
		if(_Connection._ActiveTransaction != this)
			throw new TransactionExistsException();
		_Connection._ActiveTransaction = null;
	}
}