module ShardData.DbCommand;
private import std.stdio;
public import ShardData.DbCachePolicy;
private import std.string;
private import std.conv;
private import ShardTools.ExceptionTools;
public import ShardData.Query;
public import ShardData.DataRow;
public import ShardData.DbDataSet;
public import ShardData.DbParameter;
import ShardData.DbConnection;
import std.exception;
import ShardTools.Untyped;

mixin(MakeException("CommandFailedException", "Attempting to execute a command resulted in an error."));
mixin(MakeException("CommandPreparedException", "The command was already prepared and not able to be changed as a result."));

/// Represents a single command, or query, to the database.
abstract class DbCommand  {

	// TODO: Fix cache when manually changing parameters instead of just using exec.

public:
	
	//mixin(Mixins.CreateLowercaseAliases!(typeof(this)));

	/// Initializes a new instance of the DbCommand object.
	this(DbConnection Connection, string CommandText = null) {
		// TODO: Varargs parameters. Maybe.		
		this._Connection = Connection;
		this.CommandText = CommandText;		
	}

	/// Gets the connection being used for this command.
	@property DbConnection Connection() {
		return _Connection;
	}

	/// Gets or sets the command text to execute.
	@property string CommandText() {
		return this._CommandText;
	}

	/// Ditto
	@property void CommandText(string Value) {
		synchronized(this) {
			this._CommandText = Value;
		}
	}

	/// Gets the parameters that this command contains.
	@property DbParameter[] Parameters() {
		return _Parameters;
	}

	/// Gets or sets the cache policy used when executing this command, or null for no caching.
	@property DbCachePolicy CachePolicy() {		
		return _CachePolicy;		
	}

	/// Ditto
	@property void CachePolicy(DbCachePolicy Policy) {		
		synchronized(this) {
			this._CachePolicy = Policy;		
		}
	}

	/// Creates a parameter with the given name.
	/// The parameter is not added to the command yet.
	abstract DbParameter CreateParameter(string Name);	

	/// Adds the given parameter to the parameter collection.
	/// Params:
	/// 	Parameter = The parameter to add to the parameter collection.
	void AddParameter(DbParameter Parameter) {		
		synchronized(this) {
			this._Parameters ~= Parameter;
			if(_CachePolicy)
				_CachePolicy.Invalidate();
		}
	}

	/// Adds parameters with the given names to this command.
	final void AddParameter(string[] Names ...) {
		foreach(string Name; Names) {
			DbParameter Param = CreateParameter(Name);
			AddParameter(Param);
		}			
	}

	private DbDataSet ExecuteCommon(T...)(T Params) {		
		// TODO: Have cache policy cache by parameters.
		if(Connection is null || Connection.State != DbConnectionState.Open)
			throw new ConnectionStateException("Unable to execute a command without an open connection.");
		bool CacheInvalid = this.CachePolicy is null;
		bool ParamsChanged = false;
		static if(Params.length)
			ParamsChanged = SetParams!(T)(Params) || CacheInvalid;						
		return GetCachedSet(ParamsChanged);		
	}

	protected DbDataSet GetCachedSet(bool ParametersChanged) {		
		synchronized(this) {
			if(this.CachePolicy !is null && !ParametersChanged)
				return this.CachePolicy.ResultSet;
			if(this.CachePolicy !is null && ParametersChanged)
				this.CachePolicy.Invalidate();
			return null;		
		}
	}

	protected void OnResultReceived(DbDataSet Set) {	
		synchronized(this) {
			// TODO: BUG: Potential race condition here! What if parameters were changed and we got a second result and created a result set for it before creating one for the first one?
			// Don't think that's actually possible right now.
			if(this.CachePolicy !is null) {
				//debug writeln("Caching result set.");
				this.CachePolicy.ResultSet = Set;
			}		
		}
	}

	/// Executes this command, optionally assigning parameters in the order provided.
	/// If there are currently no parameters set and this is the first execute, an implicit parameter named Param1 through ParamN will be created and added for each argument.
	/// No asynchronous method exists; use ExecuteAsync and get the Count instead.
	final ulong ExecuteScalar(T...)(T Params) {	
		DbDataSet Set = ExecuteCommon(Params);		
		if(Set !is null) {
			size_t Result = Set.Count;
			Set.Close();
			return Result;
		}
		return PerformExecuteScalar();	
	}

	/// Executes this command, optionally assingning parameters in the order specified, then returns the result as a DataSet.
	/// If there are currently no parameters set and this is the first execute, an implicit parameter named Param1 through ParamN will be created and added for each argument.
	/// The Async version does the same thing, but does so asynchronously and returns a DbAction that can be used to get the DbDataSet completion data afterwards (or a Throwable if an error occurred).
	final DbDataSet Execute(T...)(T Params) {		
		DbDataSet Set = ExecuteCommon(Params);
		if(Set)			
			return Set;		
		DbDataSet Result = PerformExecute();
		OnResultReceived(Result);
		return Result;
	}

	/// Ditto
	final DbAction ExecuteAsync(T...)(T Params) {
		DbDataSet Set = ExecuteCommon(Params);
		if(Set) {
			DbAction Action = new DbAction(this.Connection);
			Action.NotifyComplete(CompletionType.Successful, cast(void*)Set);
			return Action;
		}		
		DbAction Result = PerformExecuteAsync();
		Result.NotifyOnComplete(&OnAysncResultComplete);
		return Result;
	}

	private void OnAysncResultComplete(void* State, AsyncAction Action, CompletionType Type) {
		if(Type == CompletionType.Successful)
			OnResultReceived(cast(DbDataSet)Action.CompletionData);
	}

	private bool SetParams(T...)(T Params) {
		synchronized(this) {
			bool AnyChanged = false;		
			if(FirstExecute && Parameters.length == 0) {
				foreach(Index, Argument; Params) {
					DbParameter Param = CreateParameter("Param" ~ to!string(Index + 1));
					AddParameter(Param);				
					AnyChanged = true;
				}		
			}

			FirstExecute = false;
			enforce(Params.length == Parameters.length, "If assigning parameters upon execute, must assign all parameters.");		
			// TODO: Cache multiple values, and decide a better way of doing this comparison.
			if(CachePolicy !is null) {						
				TypeInfo[] NewTypeData = new TypeInfo[Params.length];
				TypeInfo[] CachedTypeData = this._CachedParameters.TypeData;	
				if(CachedTypeData.length != Params.length) {				
					// This will occur when we change our cache policy, or have not cached yet.
					// So, we populate the cache.
					auto CachedData = new Tuple!(T)(T.init);
					foreach(Index, Value; Params) {
						NewTypeData[Index] = typeid(typeof(Value));
						(*CachedData)[Index] = Value;
						DbParameter Param = Parameters[Index];
						Param.Value = Variant(Value);
					}				
					this._CachedParameters.CachedData = CachedData;
					this._CachedParameters.TypeData = NewTypeData;
					return true;
				} 

				Tuple!(T)* NewData = new Tuple!(T)(T.init);
				Tuple!(T)* CachedData = cast(Tuple!(T)*)this._CachedParameters.CachedData;				
			
				// Go through each of the parameters, populate the type data and new value, then compare to the old value.
				// If they all match the old values, we can use our cached result set.
				foreach(Index, Value; Params) {
					TypeInfo CurrType = typeid(typeof(Value));
					NewTypeData[Index] = CurrType;				
					(*NewData)[Index] = Value;				
					if(CachedTypeData[Index] != CurrType) {						
						AnyChanged = true;
						// Continue so we cache.												
					}				
					if((*CachedData)[Index] != Value) {					
						Parameters[Index].Value = Variant(Value);
						(*CachedData)[Index] = Value;
						AnyChanged = true;
					}
				}					
				return AnyChanged;
			}	
			foreach(Index, Param; Params) {			
				Parameters[Index].Value = Variant(Param);
				AnyChanged = true;			
			}
			return AnyChanged;
		}
	}

	/// Convenience Method - Executes this command, returning the first row of the resulting dataset.
	/// If there are currently no parameters set and this is the first execute, an implicit parameter named Param1 through ParamN will be created and added for each argument.
	final DataRow ExecuteSingleRow(T...)(T Params) {
		DbDataSet Set = Execute(Params);
		scope(exit)
			Set.Close();
		DataRow Result = Set.Read();
		return Result;		
	}

	final DbParameter opIndex(string Index) {
		synchronized(this) {
			foreach(DbParameter Param; _Parameters)
				if(icmp(Param.Name, Index) == 0)
					return Param;
			return null;
		}
	}

	final DbParameter opIndex(size_t Index) {
		synchronized(this) {
			return Parameters[Index];
		}
	}		

protected:
	/// Override to handle the execution of this command.
	/// By default, calls PerformExecute, gets the count, destroys it, then returns the count.
	ulong PerformExecuteScalar() {		
		DbDataSet Set = PerformExecute();
		scope(exit)
			Set.Close();
		ulong Result = Set.Count;
		OnResultReceived(Set);
		return Result;
	}

	/// Executes this query synchronously.
	/// The default implementation calls the asynchronous version and waits for completion.
	/// If the asynchronous implementation did not succeed, the CompletionData is thrown, or a generic CommandFailedException if no Throwable is assigned to CompletionData.
	DbDataSet PerformExecute() {		
		DbAction Action = PerformExecuteAsync();
		CompletionType Type = Action.WaitForCompletion();
		Untyped CompletionData = Action.CompletionData;
		if(Type != CompletionType.Successful) {			
			Throwable AsThrow = cast(Throwable)CompletionData;
			if(AsThrow)
				throw AsThrow;
			throw new CommandFailedException();			
		}
		DbDataSet Set = cast(DbDataSet)CompletionData;		
		return Set;		
	}

	/// Override to handle executing the command and returning a DataSet.
	abstract DbAction PerformExecuteAsync();		

	/// Replaces the current parameters with the given values for this command.
	/// Params:
	/// 	Parameters = The parameters to set for this command.
	@property protected void Parameters(DbParameter[] Parameters) {
		// TODO: Caching.
		synchronized(this) {
			this._Parameters = Parameters;
		}
	}

private:
	struct CacheDetails {
		TypeInfo[] TypeData;
		void* CachedData;
	}

	DbParameter[] _Parameters;
	string _CommandText;
	DbConnection _Connection;
	DbCachePolicy _CachePolicy;
	CacheDetails _CachedParameters;
	bool FirstExecute = true;		
}