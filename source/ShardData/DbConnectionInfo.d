﻿module ShardData.DbConnectionInfo;
public import ShardData.IDbConnectionPool;

enum SslMode {
	Prefer = 0,
	PreferNot = 1,
	Require = 2,
	Never = 3
}

/// Provides information on how to connect to a database.
/// Note that for connection pooling to work properly, all derived classes $(B, must) implement opEquals and opHash, allowing them to be used in an associative array.
/// This class is not thread safe.
class DbConnectionInfo  {

public:
	/// Initializes a new instance of the ConnectionInfo object.
	this(string Username = null, string Password = null, string Host = null, string Database = null, ushort Port = 0) {
		this.Username = Username;
		this.Password = Password;
		this.Host = Host;
		this.Database = Database;
		this.Port = Port;
	}

	/// Gets or sets the username to authenticate with.
	@property string Username() const {
		return _Username;
	}

	/// Ditto
	@property void Username(string Username) {
		this._Username = Username;
	}

	/// Gets or sets the password to use for authentication.
	@property string Password() const {
		return _Password;
	}

	/// Ditto
	@property void Password(string Password) {
		// TODO: Possibly encrypt password internally. Have getter decrypt.
		this._Password = Password;
	}

	/// Gets or sets the host to connect to.
	@property string Host() const {
		return _Host;
	}
	
	/// Ditto
	@property void Host(string Host) {
		this._Host = Host;
	}

	/// Gets or sets the default database to use.
	@property string Database() const {
		return this._Database;
	}

	/// Ditto
	@property void Database(string Value) {
		this._Database = Value;
	}

	/// Gets or sets the port to attempt a connection on.
	@property ushort Port() const {
		return this._Port;
	}

	/// Ditto
	@property void Port(ushort Value) {
		this._Port = Value;
	}

	/// Gets or sets whether to use SSL encryption for this connection.
	@property SslMode SSL() const {
		return _SSL;
	}

	/// Ditto
	@property void SSL(SslMode Value) {
		_SSL = Value;
	}

	/// Gets or sets whether to compress data returned.
	@property bool UseCompression() const {
		return _UseCompression;
	}

	/// Ditto
	@property void UseCompression(bool Value) {
		_UseCompression = Value;
	}

	/+
	// TODO: Decide on a good way to do automatic connection pooling.
	/// Gets a pool used to store this connection info.
	@property IDbConnectionPool GetPool() {

	}+/
	
private:	
	string _Username;
	string _Password;
	string _Host;
	string _Database;
	ushort _Port;
	SslMode _SSL;
	bool _UseCompression;
}