﻿/// Indicates a transaction that should be rolled back if the current scope is left prior to it being committed.
/// When a LocalTransaction goes out of scope, the Rollback function is called (but no exception is thrown).
/// ---
/// auto Trans = Connection.StartTransaction();
/// auto RowsReturned = Command.ExecuteScalar();
/// // No need to worry about the below exception because we automatically rollback when scope is left.
/// if(RowsReturned <= 1)
///		throw new InsufficientDataException();
/// InsertCommand.Update(RowsReturned);
/// Trans.Commit();
/// ---
/// LocalTransactions may not be copied nor assigned, but may be extended using ExtendLifespan.
/// If ExtendLifespan is called, the underlying DbTransaction is returned, and this LocalTransaction going out of scope does nothing.
/// ---
/// struct Appender {
///		DbTransaction Transaction;
///		this() {
///			auto LocalTransaction = Connection.StartTransaction();
///			this.Transaction = LocalTransaction.ExtendLifespan();
///		}
/// 
///		void Complete() {
///			this.Transaction.Commit();
///		}
/// }
/// ---
module ShardData.LocalTransaction;
public import ShardData.DbTransaction;

/// Wraps a transaction, rolling back the transaction upon this struct being destroyed.
struct LocalTransaction  {

public:

	@disable this();
	@disable this(this);

	// While alias this would be nice, the whole implicitly converting thing is a problem.

	/// Creates a new LocalTransaction wrapping the given DbTransaction.
	this(DbTransaction Transaction) {
		this._Transaction = Transaction;
	}
	
	/// Gets the transaction that this object wraps.
	@property DbTransaction Transaction() {
		return _Transaction;
	}

	/// Commits the underlying transaction.
	void Commit() {		
		Transaction.Commit();		
	}

	/// Rolls back the underlying transaction.
	void Rollback() {		
		_Transaction.Rollback();
	}

	/// Removes the locality of this transaction, returning the transaction itself.
	/// This wrapper may be safely destroyed after this method is called, without affecting the transaction itself.
	DbTransaction ExtendLifespan() {		
		_NoRollback = true;
		return _Transaction;
	}
	
	~this() {
		if(!_Transaction.IsComplete && !_NoRollback)
			_Transaction.Rollback();
	}

	
private:
	DbTransaction _Transaction;	
	bool _NoRollback = false;
}